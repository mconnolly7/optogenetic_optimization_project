% %%
clc;
clear;
close all;
% 
%% configure

file_path       = mfilename('fullpath');
split_idx       = strfind(file_path, filesep);
root_dir        = file_path(1:split_idx(end));
 
addpath(genpath(root_dir))

dataset        = 'optogenetic';
model_path      = {'R2_gamma_3D', 'R3_gamma_3D'};
model_dir       = [root_dir 'modeled_data/' dataset filesep];
results_dir     = [root_dir 'simulation_data/' dataset filesep];

%% Configure experiment 
n_trials        = 30;
n_samples       = 100;

%% Configure hyperparameters
ce_params       = combvec([4 5 10 20], [1 .8 .6])';

spsa_params     = combvec([1 100 500 1000], [1 50 200])';

bao_params(:,1) = [1 1 1 1 ...
                   2 2 2 2 ...
                   3 3 3 3]';
                
bao_params(:,2) = [0.01,  0.05, 0.1,  1, ...
                   0.005, 0.01, 0.1, .3,...
                   0.1,   0.2,  0.4, .6]'; 

spsa_params     = spsa_params(1,:);               
ce_params       = ce_params(1,:);               
bao_params      = [3 0.01];
rand_params     = ones(12,2);

%% run optimization
tic
for c1 = 1:size(model_path,2)
    
%     load(save_path)
    
    for c2 = 1:size(bao_params,1)
        
        x = load([model_dir model_path{c1}]);

        [spsa_pos_state{c2}, spsa_stim_params{c2}, spsa_optimal_est{c2}]    ...
            = spsa_opt(x.gp_model, n_trials, n_samples, spsa_params(c2,:));

        [ce_pos_state{c2}, ce_stim_params{c2}, ce_optimal_est{c2}]          ...
            = cross_entropy_opt(x.gp_model, n_trials, n_samples, ce_params(c2,:));
   
        tic
        [bao_pos_state{c2}, bao_stim_params{c2}, bao_optimal_est{c2}]       ...
            = bayes_opt(x.gp_model, n_trials, n_samples, bao_params(c2,:));
        t = toc;
        fprintf('Hyperparameter set %d took %fs\n', c2, t);
        
        pos_state   = [spsa_pos_state    ce_pos_state    bao_pos_state]';
        stim_params = [spsa_stim_params  ce_stim_params  bao_stim_params]';
        optimal_est = [spsa_optimal_est  ce_optimal_est  bao_optimal_est]';
        opt_params  = [spsa_params;      ce_params;      bao_params];
        optimizer   = [ones(size(spsa_params,1),1); 2*ones(size(ce_params,1),1); 3*ones(size(bao_params,1),1)];
        
        save_path = [results_dir model_path{c1} '_simulation_data_parameter_sweep_test_' num2str(c2) '.mat'];

        save(save_path, 'pos_state', 'stim_params', 'optimal_est', 'optimizer', 'opt_params')
      
    end
    
%     pos_state(1:12)     = spsa_pos_state;
%     stim_params(1:12)   = spsa_stim_params;
%     optimal_est(1:12)   = spsa_optimal_est;
%     opt_params(1:12,:)  = spsa_params;

%     pos_state   = [spsa_pos_state    ce_pos_state    bao_pos_state]';
%     stim_params = [spsa_stim_params  ce_stim_params  bao_stim_params]';
%     optimal_est = [spsa_optimal_est  ce_optimal_est  bao_optimal_est]';  
%     opt_params  = [spsa_params;      ce_params;      bao_params];   
%     optimizer   = [ones(size(spsa_params,1),1); 2*ones(size(ce_params,1),1); 3*ones(size(bao_params,1),1)];
%     
%     save(save_path, '-append', 'pos_state', 'stim_params', 'optimal_est', 'optimizer', 'opt_params')
%       
end
toc
