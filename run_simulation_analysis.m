clear; clc; close all;

dataset                 = 'optogenetic';
simulation_data_paths   = {'R2_gamma_3D_simulation_data_parameter_sweep_test_1', ...
    'R3_gamma_3D_simulation_data_parameter_sweep_test_1'};

model_paths             = {'R2_gamma_3D', 'R3_gamma_3D'};

root_dir                = './';
data_dir                = [root_dir 'simulation_data/' dataset filesep];
model_dir               = [root_dir 'modeled_data/' dataset filesep];
results_dir             = [root_dir 'simulation_results/' dataset filesep];

for c2 = 1:size(model_paths,2)
    model_path      = model_paths{c2};
    data_path       = simulation_data_paths{c2};
    save_name       = strrep(data_path,'data', 'results');
    save_path       = [results_dir save_name];

    load([data_dir data_path])
    load([model_dir model_path]);

    optimal_ground_truth    = gp_model.discrete_extrema(2);

    lower_bound             = gp_model.lower_bound;
    upper_bound             = gp_model.upper_bound;

    for c1 = 1:size(optimal_est,1)
        experiment_optimal_est  = optimal_est{c1};

        for c3 = 1:size(experiment_optimal_est,3)

            p_dist(c1,c3)       = parameter_distance(experiment_optimal_est(:,:,c3), optimal_ground_truth, lower_bound, upper_bound);
            e_dist(c1,c3)       = error_from_optimum(experiment_optimal_est(:,:,c3), optimal_ground_truth, gp_model);

            p_converge(c1,c3)   ...
                = parameter_convergence_rate(experiment_optimal_est(:,:,c3), gp_model);

            [e_converge(c1,c3), e_sample{c1,c3}, e_trace{c1,c3}]  ...
                = error_convergence_rate(experiment_optimal_est(:,:,c3), optimal_ground_truth, gp_model);

        end
    end

    save(save_path, 'p_converge', 'p_dist', 'e_converge', 'e_dist',...
        'opt_params', 'model_metadata', 'optimizer', 'e_sample', 'e_trace',...
        'optimal_est', 'stim_params', 'optimal_ground_truth')
end
