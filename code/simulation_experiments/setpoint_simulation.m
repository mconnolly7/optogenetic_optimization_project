% function setpoint_simulation(inputArg1,inputArg2)
rng(0);

% Configure experiment 

data_dir        = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/';

data_path       = [data_dir 'R' num2str(1) '_gamma.mat'];

load(data_path);
X_gt            = gp_model.x_data;
Y_gt            = gp_model.y_data;

objective_model = gp_object;
objective_model.initialize_data(X_gt, Y_gt);


%%
n_trials        = 12;
setpoints       = [.1 .5 1.2];

n_samples       = 40;
n_burn_in       = 10;
n_setpoints     = size(setpoints,2);

X_samples_all   = [];
Y_samples_all   = [];
X_optimal_all   = [];
y_gt_all        = [];

sample_idx_all  = [];
trial_idx_all   = [];
setpoint_all    = [];

% % run optimization

for c1 = 1:n_setpoints


    [X_samples, Y_samples, X_optimal_est, y_ground_truth]       ...
        = bayes_opt_loop(objective_model, n_trials, n_samples, n_burn_in, [3 .4], setpoints(c1));
    
    sample_idx          = repmat(1:n_samples, 1, n_trials)';
    trial_idx           = reshape(repmat(1:n_trials, n_samples, 1), [], 1);
    m_samples           = n_trials*n_samples;
    setpoint_mat        = setpoints(c1) * ones(m_samples,1);
    
    sample_idx_all      = [sample_idx_all; sample_idx];
    trial_idx_all       = [trial_idx_all; trial_idx];
    setpoint_all        = [setpoint_all; setpoint_mat];
    
    X_sample_1          = reshape(squeeze(X_samples(:,1,:)),[],1);
    X_sample_2          = reshape(squeeze(X_samples(:,2,:)),[],1);
    X_sample_table      = [X_sample_1 X_sample_2];
    
    X_samples_all       = [X_samples_all; X_sample_table];
    Y_samples_all       = [Y_samples_all; reshape(Y_samples, m_samples, 1)];
    
    X_optimal_1         = reshape(squeeze(X_optimal_est(:,1,:)),[],1);
    X_optimal_2         = reshape(squeeze(X_optimal_est(:,2,:)),[],1);
    X_optimal_table     = [X_optimal_1 X_optimal_2];
    
    X_optimal_all       = [X_optimal_all; X_optimal_table];
    y_gt_all            = [y_gt_all; reshape(y_ground_truth, m_samples, 1)];

end     

% end

