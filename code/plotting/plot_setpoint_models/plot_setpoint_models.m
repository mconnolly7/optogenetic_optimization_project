data_dir        = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/';
thresholds     = [.15 .45 .8];
close all

x_grid      = [.09 .55];
y_grid      = [.6 .16];
width       = .37;
height      = .34;
f           = figure( 'Position',  [190, 100, 900, 700]);

ax(1)        	= axes('Position', [x_grid(1) y_grid(1) width height]);
ax(2)        	= axes('Position', [x_grid(2) y_grid(1) width height]);
ax(3)        	= axes('Position', [x_grid(1) y_grid(2) width height]);
ax(4)        	= axes('Position', [x_grid(2) y_grid(2) width height]);

hold(ax(1),'on');hold(ax(2),'on');hold(ax(3),'on');hold(ax(4),'on');

d1              = [253,187,132; 239,101,72; 153,0,0]/255; 

font_size       = 14;
n_samples       = 100;
subject_idx     = 1;
data_path       = [data_dir 'R' num2str(subject_idx) '_gamma.mat'];
load(data_path);

X_gt            = gp_model.x_data;
Y_gt            = gp_model.y_data;

objective_model = gp_object;
objective_model.initialize_data(X_gt, Y_gt);

input_1         = linspace(objective_model.lower_bound(1), objective_model.upper_bound(1), n_samples);
input_2         = linspace(objective_model.lower_bound(2), objective_model.upper_bound(2), n_samples);

input_space     = combvec(input_1, input_2)';
for c1 = 1:size(thresholds,2)
    
    set(f, 'currentaxes', ax(c1));
    Y_est           = -1*abs(objective_model.predict(input_space) - thresholds(c1));
    
    Y_est_r         = reshape(Y_est, n_samples, n_samples)';
    input_r1        = reshape(input_space(:,1), n_samples, n_samples)';
    input_r2        = reshape(input_space(:,2), n_samples, n_samples)';
    
    surf(input_r1, input_r2, 1*Y_est_r, 'EdgeColor', 'none', 'FaceAlpha', .7)
    
    view(0,90)
    xlim([10 50])
    ylim([5 42])
    colormap('hot')
    set(gca, 'FontSize', font_size);
    title(sprintf('Setpoint = %.2f', thresholds(c1)))
end

%%%%%%%%%%%
set(f, 'currentaxes', ax(4));

markers         = {'s', '^', 'o'};
n_order         = [3 3 3; 4 4 3; 4 3 3];
n_elite         = 100*ones(3);
line_style      = {'-',':', '--'};
hold(ax(4),'on')

for c2 = 1:3
    for c1 = 1:3
        
        data_path       = [data_dir 'R' num2str(c1) '_gamma.mat'];

        load(data_path);
        X_gt            = gp_model.x_data;
        Y_gt            = gp_model.y_data;

        objective_model = gp_object;
        objective_model.initialize_data(X_gt, Y_gt);

        input_1         = linspace(objective_model.lower_bound(1), objective_model.upper_bound(1), n_samples);
        input_2         = linspace(objective_model.lower_bound(2), objective_model.upper_bound(2), n_samples);
        
        input_space     = combvec(input_1, input_2)';
        Y_est           = -1*abs(objective_model.predict(input_space) - thresholds(c2));

        [y_est_sort, y_est_sort_idx]    = sort(Y_est, 'descend');
        input_focus                     = input_space(y_est_sort_idx(1:n_elite(c1,c2)),:);
        
        [~, sort_input_focus_idx]       = sort(input_focus(:,2));
        input_focus_sort                = input_focus(sort_input_focus_idx,:);       

        p               = polyfit(input_focus_sort(:,2), input_focus_sort(:,1), n_order(c1,c2));
        x_iso_2{c1,c2}  = linspace(min(input_focus_sort(:,2)), max(input_focus_sort(:,2)), 100);
        x_iso_1{c1,c2}  = polyval(p, x_iso_2{c1,c2});
        
        plot(ax(4),x_iso_2{c1,c2}, x_iso_1{c1,c2},'color', d1(c2,:), 'LineWidth', 3, 'LineStyle', line_style{c1})
        hold on

        view(90,270)
    end
end

box off
set(gca, 'FontSize', font_size);

set(f, 'currentaxes', ax(1));
plot3(x_iso_1{1,1}, x_iso_2{1,1}, ones(size(x_iso_1{1,1})), 'color', zeros(1,3), 'LineWidth', 5, 'LineStyle', line_style{1})
caxis([-.8 0])

set(f, 'currentaxes', ax(2));
plot3(x_iso_1{1,2}, x_iso_2{1,2}, ones(size(x_iso_1{1,1})), 'color', zeros(1,3), 'LineWidth', 5, 'LineStyle', line_style{1})
c               = colorbar('Position',[ .93 .6 .01 height(1)]);
c.Label.String  = 'Gamma Power (scaled)]';
c.Label.FontSize = font_size;
caxis([-.8 0])

set(f, 'currentaxes', ax(3));
plot3(x_iso_1{1,3}, x_iso_2{1,3}, ones(size(x_iso_1{1,1})), 'color', zeros(1,3), 'LineWidth', 5, 'LineStyle', line_style{1})
xlabel(ax(1),'Amplitude (mW/mm^2)')
ylabel(ax(1),'Frequency (Hz)')
set(f, 'currentaxes', ax(4));

ylim([10 50])
xlim([5 42])

legend(ax(4),{'R1 Y_{SP} = 0.15','R2 Y_{SP} = 0.15','R3 Y_{SP} = 0.15',...
    'R1 Y_{SP} = 0.45','R2 Y_{SP} = 0.45','R3 Y_{SP} = 0.45',...
    'R1 Y_{SP} = 0.8','R2 Y_{SP} = 0.8','R3 Y_{SP} = 0.8'},'NumColumns',3, ...
    'Box', 'off','FontSize',12, 'Position', [.48 .03 .5 .1])
y_coords        = [.9  .47 ];
x_coords        = [.06 .5];
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

print('setpoint_models', '-depsc')

