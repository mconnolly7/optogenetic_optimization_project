function [x_max_est, y_max_est] = get_optimization_trajectory(gp_model)

X           = gp_model.x_data;
Y           = gp_model.y_data;
lower_bound = gp_model.lower_bound;
upper_bound = gp_model.upper_bound;

n_burn_in   = 10;

for c1 = n_burn_in:size(Y,1)
    x_update        = X(1:c1,:); 
    y_update        = Y(1:c1);

    gp_update       = opto_gp_object;
    gp_update.initialize_data(x_update, y_update, lower_bound, upper_bound);

    [x_max, y_max]  = gp_update.discrete_extrema(2);

    x_max_est(c1,:) = x_max;
    y_max_est(c1,:) = y_max;

end
end

