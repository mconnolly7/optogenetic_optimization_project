function [y_train_all, y_all_test] = plot_optimization(parameter_space, a, b, c, e)

p = [171,217,233
    43,131,186]/255;

subject_idx         = 4;
gp_model_dir        = 'optogenetic_optimization_project/data/in_vivo_data/ms_opto_theta_maximization/gp_model_data/';

% Get subject list
d = dir([gp_model_dir 'E*']);

for c1 = 1:size(d,1)
    dir_name{c1}        = d(c1).name;   
    
    dir_name_split      = split(dir_name{c1}, '-');
    subject{c1}         = dir_name_split{1};
    date{c1}            = dir_name_split{2};
    parameter{c1}       = dir_name_split{3}(1:end-4);
end

subjects            = unique(subject);
subject_rep_idx     = find(strcmp(subject,subjects{subject_idx}));

parameter_rep_idx   = find(strcmp(parameter,parameter_space));
plot_idx            = intersect(subject_rep_idx, parameter_rep_idx);
    
y_train_all = [];

for c1 = 1:size(plot_idx, 2)
    load([gp_model_dir dir_name{plot_idx(c1)}]);
    X = gp_temp.x_data;
    Y = gp_temp.y_data;
    
    n_burn_in   = 10;
    switch parameter_space
        case 'standard'  
            lower_bound = [2  .001];
            upper_bound = [100 .008];
        case 'nested'
            lower_bound = [35 5];
            upper_bound = [100 11];
    end
            
    
    for c2 = n_burn_in+1:size(Y,1)
        x_update        = X(1:c2,:); 
        y_update        = Y(1:c2);

        gp_update       = opto_gp_object;
        gp_update.initialize_data(x_update, y_update, lower_bound, upper_bound);

        [x_max, y_max]  = gp_update.discrete_extrema(2);

        x_max_est(c2,:) = x_max;
        y_max_est(c2,:) = y_max;

    end
    
    t_est = (n_burn_in + 1):size(x_max_est,1);
    subplot(6,2,a)
    plot(t_est, x_max_est(n_burn_in + 1:end,1), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
    hold on
    plot(X(:,1), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
    
    subplot(6,2,b)
    plot(t_est, x_max_est(n_burn_in + 1:end,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-')
    hold on
    plot(X(:,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
    
    subplot(6,2,c)
    plot(t_est, y_max_est(n_burn_in + 1:end,1), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-')
    hold on
    
    y_train_all = [y_train_all; y_max_est];
end

subplot(6,2,a)
ylabel('Pulse Frequency')
set(gca, 'Fontsize', 16)
xlim([0 50])
xticklabels({})

subplot(6,2,b)
switch parameter_space
    case 'standard'  
        ylabel('Pulse Width')
    case 'nested'
        ylabel('Train Frequency')
end
set(gca, 'Fontsize', 16)
xticklabels({})
xlim([0 50])

subplot(6,2,c)
ylabel('Change in Theta Power')
xlabel('Samples')
set(gca, 'Fontsize', 16)
xlim([0 50])

subplot(6,2,e)
y_all_test = plot_test_phase(parameter_space);

end
