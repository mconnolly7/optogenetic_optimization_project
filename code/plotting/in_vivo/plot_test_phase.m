function y_all = plot_test_phase(parameter_space)

close all

color_p = [215,25,28
253,174,97
171,221,164
43,131,186]/255;

% test_data           = ['in_vivo_optimization/data/ms_opto_theta_maximization/test_data/' parameter_space '_pulse' filesep 'Normal/'];

test_data           = 'optogenetic_optimization_project/data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/';

% Get subject list
d = dir([test_data 'E*.mat']);

for c1 = 1:size(d,1)
    file_name{c1}        = d(c1).name;   
    
    file_name_split      = split(file_name{c1}, '_');
    subject{c1}         = file_name_split{1};
    date{c1}            = file_name_split{2};
    parameter{c1}       = file_name_split{3}(1:end-4);
end

subjects            = unique(subject);
x_all               = [];
y_all               = [];

hold on
for c1 = 2:size(subjects,2)
    subject_rep_idx     = find(strcmp(subject,subjects{c1}));
    
    for c2 = 1:size(subject_rep_idx,2)
        file_path           = [test_data file_name{subject_rep_idx(c2)}]; 

        load(file_path);
        x = [1:3] + rand(1,3)*.2 - .1;
%         scatter(x, mean(OF_diff_osbs,2), 200, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', color_p(c1,:))
       
        x_all = [x_all x];
%         y_all = [y_all; mean(OF_diff_osbs,2)];
        y_all = [y_all; mean(OF_sort_os,2)];
    end
end

h = boxplot(y_all, round(x_all'), 'notch', 'off');

for c1 = 2:size(subjects,2)
    subject_rep_idx     = find(strcmp(subject,subjects{c1}));
    
    for c2 = 1:size(subject_rep_idx,2)
        file_path           = [test_data file_name{subject_rep_idx(c2)}]; 

        load(file_path);
        x = [1:3] + rand(40,3)*.2 - .1;
%         scatter(x, mean(OF_diff_osbs,2), 200, 'MarkerEdgeColor', 'k', 'MarkerFaceColor', color_p(c1,:))
       
        scatter(reshape(x,[],1), reshape(OF_sort_os',[],1), 200, 'MarkerEdgeColor',...
           'k', 'MarkerFaceColor', color_p(c1,:), 'MarkerFaceAlpha', .5)

    end
end

set(h,{'linewidth', 'color'},{4, 'k'})

xticks([1 2 3])
xticklabels({'Sham', 'Optimal', 'Worst'});
ylabel('Gamma Power (V^2/Hz)')

set(gca, 'FontSize', 16)

xlim([.5 3.5])
% ylim([-4.4715    7.5508]*1.1)

end

