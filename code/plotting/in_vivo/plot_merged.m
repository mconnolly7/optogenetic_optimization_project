clc; close all
parameter_space     = 'standard';
[y_all_train_1, y_all_test_1] = plot_optimization(parameter_space, 1, 3, 5, [7 9 11])
y_all_test = plot_test_phase(parameter_space);


parameter_space     = 'nested';
[y_all_train_2, y_all_test_2] = plot_optimization(parameter_space, 2, 4, 6, [8 10 12])

y_train = [y_all_train_1; y_all_train_2]*1.1;
y_test  = [y_all_test_1; y_all_test_2]*1.1;

subplot(6,2,5)
ylim([min(y_train), max(y_train)])

subplot(6,2,6)
ylim([min(y_train), max(y_train)])

% hold on
% subplot(3,4,[2 6 10])
% ylim([min(y_test), max(y_test)])
% 
% subplot(3,4,[4 8 12])
% ylim([min(y_test), max(y_test)])