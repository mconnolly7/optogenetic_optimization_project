clear;clc;close all;
paper_dir           = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir      = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/'];
test_data_dir       = [experiment_dir 'test_data/'];
raw_data_dir        = [test_data_dir 'raw_data/'];
processed_data_dir  = [test_data_dir 'processed_data/'];

param_idx = 3;
params_vec = combvec([1 2], [1 2 3])';
subject_data        = {'EPI033' 'EPI032'};
p_color = [231,41,138
            102,166,30
            230,171,2]/255;

subject_idx         = params_vec(param_idx,1);
trial_idx           = params_vec(param_idx,2);
channel             = 3;
sampling_rate       = 2000;
subject_data_dir    = [raw_data_dir subject_data{subject_idx} '/'];

d = dir([subject_data_dir 'EPI*']);

trial_data_path = [subject_data_dir d(trial_idx).name];

d_sham          = dir([trial_data_path '/Sham/Re*']);
d_optimal       = dir([trial_data_path '/Optimal/Re*']);
d_suboptimal    = dir([trial_data_path '/Suboptimal/Re*']);


subject_data        = {'EPI033' 'EPI032'};

line_style          = {'--', ':','-'};
color_p = [43,131,186
    215,25,28
    171,217,233]/255;

hold on
sample_idx      = 5;
t               = (1:15*sampling_rate)/sampling_rate;

data_path       = [d_sham(sample_idx).folder filesep d_sham(sample_idx).name];
load(data_path, 'data');
data_seg        = data(channel,1:15*sampling_rate);
data_band       = bandpass(data_seg,[4 100], sampling_rate);
plot_idx(1)     = plot(t, data_band + 1e-3, 'color', p_color(1,:), 'LineWidth', 1);




%%
close all
gamma           = sin(t * 42*2*pi);
params.Fs       = 2000;
params.tapers   = [3 5];
params.fpass    = [1 100];

% plot(t,gamma);
t               = (1:3*sampling_rate)/sampling_rate;
t2              = t(2001:4000);
data_sham       = data(channel,1:3*sampling_rate);
data_sham       = data_sham - mean(data_sham);
data_mid        = data(channel,2:3*sampling_rate);
data_pos        = data(channel,3:4*sampling_rate);

% data_plot   = [data_pre data_mid data_pos];

data_stim = data_sham;
data_stim(2001:4000)  = data_stim(2001:4000) + gamma(1:2000)/5000;

% subplot(2,2,3)

params.fpass    = [1 100];


f = [7 10 33];
S = [1 .3  .1];

S2 = [.1 .7 1.5];

%
width = .8;
height = 0.9;
x_grid = 0.05;
y_grid = 0.05;
figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', zeros(1,3))
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])
% print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_1', '-dpng', '-r300')
% 
%

figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', zeros(1,3))
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

% print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_2', '-dpng', '-r300')

%

figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color', zeros(1,3))
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

% print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_3', '-dpng', '-r300')

%

figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color', ones(1,3)*0.7)

x_stim = x_e;

plot(t, sum(x_stim), 'color', [127 0 0]/255, 'LineWidth', 3)
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

% print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_total', '-dpng', '-r300')

% [S, f] = mtspectrumc( sum(x_stim), params);
%%
figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);

plot(t, sum(x_stim), 'color', [127 0 0]/255, 'LineWidth', 3)

xticks([])
xticklabels([])
yticks([])
yticklabels([])
box off
% set(gca,'visible','on')
set(gca,'Color','k')
print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_black', '-dtiff', '-r300')

%%
figure( 'Position',  [100, 100, 1500, 500])
hold on
plot(f,log(S), 'color', 'k', 'LineWidth', 3)
print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_increase_fft', '-dpng', '-r300')

ff = 33:50;
fff = [ff flip(fff)];
% SS  = [-10*ones(size(ff)), flip(
%%
close all
%
figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color',  ones(1,3)*0.7)

plot(t2,S2(1)*gamma(2001:4000), 'k')
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_stim_1', '-dpng', '-r300')

%
figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color',  ones(1,3)*0.7)

plot(t2,S2(2)*gamma(2001:4000), 'k')
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_stim_2', '-dpng', '-r300')

%
figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color',  ones(1,3)*0.7)

plot(t2,S2(3)*gamma(2001:4000), 'k')
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_stim_3', '-dpng', '-r300')

%

figure( 'Position',  [100, 100, 1500, 500])
ax(1)        	= axes('Position', [x_grid(1)  y_grid(1) width(1) height(1)]);
hold on
x(1,:) = sqrt(S(1))*sin(t * f(1)* 2*pi) ;
x_e(1,:) = x(1,:) + randn(size(t))/5  ;
plot(t,x(1,:) ,'color', ones(1,3)*0.7)

x(2,:) = sqrt(S(2))*sin(t * f(2)* 2*pi) ;
x_e(2,:) = x(2,:) + randn(size(t))/5  ;
plot(t,x(2,:) ,'color', ones(1,3)*0.7)

x(3,:) = sqrt(S(3))*sin(t * f(3)* 2*pi) ;
x_e(3,:) = x(3,:) + randn(size(t))/5  ;
plot(t,x(3,:) ,'color', ones(1,3)*0.7)

plot(t2,S2(3)*gamma(2001:4000),'color', ones(1,3)*0.7)

x_stim = x_e;
x_stim(3, 2001:4000) = x_stim(3, 2001:4000)+ S2(3)*gamma(2001:4000);

plot(t, sum(x_stim), 'color', [127 0 0]/255, 'LineWidth', 3)
ylim([-4 4 ])

xticks([])
xticklabels([])
yticks([])
yticklabels([])

print('/Users/mconn24/Box Sync/dissertation/defense/figure/gamma/gamma_stim_total', '-dpng', '-r300')


%%
[S, f] = mtspectrumc( sum(x_stim), params);


figure( 'Position',  [100, 100, 1500, 500])
hold on
plot(f,S)





