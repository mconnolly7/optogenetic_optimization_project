gamma_path      = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_3D/R3_gamma_3D.mat';
close all
load(gamma_path)

gamma_1D        = gp_object;
x_idx           = gp_model.x_data(:,1) == 50;

x_data          = gp_model.x_data(x_idx,2);
y_data          = gp_model.y_data(x_idx);

gamma_1D.initialize_data(x_data, y_data)
gamma_1D.minimize(1);
% gamma_1D.hyperparameters.lik = -3;
gamma_1D.plot_mean
gamma_1D.plot_standard_deviation
% gamma_1D.plot_confidence_interval
gamma_1D.plot_data
box off
xlabel('Frequency')
ylabel('Normalized Gamma Power (a.u.)')
set(gca, 'FontSize', 16)

gp_model = gamma_1D;