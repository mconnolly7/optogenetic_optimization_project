clear; close all

f               = figure( 'Position',  [100 500 800 500]);


x_grid          = [.075 .7];
y_grid_1        = .13 ;

width           = [.6 .25];
height          = [.8 ] ;

ax(2)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(1)        	= axes('Position', [x_grid(2)  y_grid_1(1) width(2) height(1)]);

hold(ax(1),'on');hold(ax(2),'on');

dark_2  = [27,158,119
        217,95,2
        117,112,179
        231,41,138]/255;

data_path       = 'optogenetic_optimization_project/results/simulation_results/3D_merge/R2_merge.mat';
load(data_path);

gamma_path      = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_3D/R2_gamma_3D.mat';
load(gamma_path)

optimal_X_001 = optimal_est{end};
optimal_X_040 = optimal_est{35};

for c1 = 1:size(optimal_X_040,3)
    X_trace_001(:,:,c1)      = squeeze(optimal_X_001(1:30,2:4,c1));
    X_trace_040(:,:,c1)      = squeeze(optimal_X_040(1:30,2:4,c1));
    optimal_gt_001(:,c1)     = gp_model.predict(X_trace_001(:,:,c1));
    optimal_gt_040(:,c1)     = gp_model.predict(X_trace_040(:,:,c1));
end

max_value           = max(max(optimal_gt_040));
error_trace_040     = max_value - optimal_gt_040;
error_trace_001     = max_value - optimal_gt_001;

set(f, 'currentaxes', ax(1));
h = boxplot([error_trace_040(end,:); error_trace_001(end,:)]');
box off


xticklabels({ 'UCB(0.4)', 'UCB(0.01)'})

ylim([0 .6])
set(h(:,1), 'MarkerEdgeColor', dark_2(3,:));
set(h(:,1), 'color', dark_2(3,:));
set(h(:,2), 'MarkerEdgeColor', dark_2(4,:));
set(h(:,2), 'color', dark_2(4,:));
set(h, 'LineWidth', 2);
yticklabels({})

set(gca,'FontSize', 16)

plot(1, mean(error_trace_040(end,:)),'x', 'markersize',15, 'color', dark_2(3,:),'LineWidth',4)
plot(2, mean(error_trace_001(end,:)),'x', 'markersize',15, 'color', dark_2(4,:),'LineWidth',4)

set(f, 'currentaxes', ax(2));

t       = 1:30;
tt      = [t flip(t)];
m       = mean(error_trace_040,2);
s       = std(error_trace_040,[],2);
se      = s / sqrt(30);

yy      = [m-se; flip(m+se)];
patch(tt,yy, dark_2(3,:), 'EdgeColor', 'none', 'FaceAlpha', .7)
plot(t,m, 'LineWidth', 2, 'color', dark_2(3,:));
ylim([0 .6])

% 001
t       = 1:30;
tt      = [t flip(t)];
m       = mean(error_trace_001,2);
s       = std(error_trace_001,[],2);
se      = s / sqrt(30);

yy      = [m-se; flip(m+se)];
patch(tt,yy, dark_2(4,:), 'EdgeColor', 'none', 'FaceAlpha', .7)
plot(t,m, 'LineWidth', 2, 'color', dark_2(4,:));
xlim([1 30])
ylabel('Error Final')

xlabel('Sample')
set(gca,'FontSize', 16)

x_bar       = [20 23];
x_patch     = [x_bar flip(x_bar)];
x_text      = 24;

plot(x_bar, [0.45 0.45], 'LineWidth', 3, 'color',  dark_2(4,:))
patch(x_patch, [0.43 0.43 .47 .47],  dark_2(4,:), 'FaceAlpha', 0.4, 'EdgeColor', 'none')
text(x_text, .45, 'UCB(0.01)', 'FontSize', 14)

plot(x_bar, [0.39 0.39], 'LineWidth', 3, 'color', dark_2(3,:))
patch(x_patch, [0.37 0.37 .41 .41], dark_2(3,:), 'FaceAlpha', 0.4, 'EdgeColor', 'none')
text(x_text, .39, 'UCB(0.4)', 'FontSize', 14)

y_coords        = [.9 ];
x_coords        = [.02 .68];
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);


print('configuration_in_silico', '-dpng', '-r500')






