% %% Organize paths for UCB(0.01)
clear; clc;
paper_dir               = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.01/'];
optimization_data_dir   = [experiment_dir 'optimization_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

data_dir{1}             = [processed_data_dir 'EPI033-GP/2018_11_14'];


experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/'];
optimization_data_dir   = [experiment_dir 'optimization_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

data_dir{2}             = [processed_data_dir 'EPI033-GP/2018_11_16'];
data_dir{3}             = [processed_data_dir 'EPI033-GP/2018_11_19'];
data_dir{4}             = [processed_data_dir 'EPI033-GP/2018_11_20'];

for c1 = 1:size(data_dir,2)        

   experiment_path = data_dir{c1};

   load([experiment_path filesep 'gp_model.mat'])

   [x_max_est, y_max_est]       = get_optimization_trajectory(gp_temp);

   plot_data(c1).x_sample       = gp_temp.x_data;
   plot_data(c1).y_sample       = gp_temp.y_data;

   plot_data(c1).x_max_est      = x_max_est;
   plot_data(c1).y_max_est      = y_max_est;

end

%%
results_dir   = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.01/';
figure_3_data = plot_data';
save([results_dir 'figure_S3_data.mat'], 'figure_3_data');


%%
close all
intensity_map       = [43.8782, -169.9840
                    48.4936, -184.5139];

f               = figure( 'Position',  [100, 100, 1000, 700]);

x_grid          = [.07 .55];
y_grid_1        = [.73 .52 .31 .1];

width           = .4;
height          = [.2 .83] ;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(1)  y_grid_1(3) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(1)  y_grid_1(4) width(1) height(1)]);
ax(5)        	= axes('Position', [x_grid(2)  y_grid_1(4) width(1) height(2)]);


results_dir   = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.01/';
load([results_dir 'figure_S3_data.mat'], 'figure_3_data');


dark_2  = [27,158,119
        217,95,2
        117,112,179
        231,41,138]/255;

t_estimate = 10:30;

plot_order = [1 2 3 4];

for c1 = 1:4
    
    plot_idx = plot_order(c1);
    
    plot_data = figure_3_data(plot_idx);
    
    if plot_idx == 1
        plot_color = dark_2(4,:);
    else
        plot_color = dark_2(3,:);
    end
    
    set(f, 'currentaxes', ax(1));
    h1(c1) = plot(t_estimate, plot_data.x_max_est(t_estimate,1) * intensity_map(1,1) + intensity_map(1,2), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', '-');
    hold on
    h2(c1) = plot(plot_data.x_sample(:,1) * intensity_map(1,1) + intensity_map(1,2), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', ':');
    
    set(f, 'currentaxes', ax(2));
    plot(t_estimate,plot_data.x_max_est(t_estimate,2), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', '-');
    hold on
    plot(plot_data.x_sample(:,2), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', ':')

    set(f, 'currentaxes', ax(3));
    plot(t_estimate, plot_data.x_max_est(t_estimate,3), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', '-');
    hold on
    plot(plot_data.x_sample(:,3), 'LineWidth', 2, 'Color', plot_color, 'LineStyle', ':')

    set(f, 'currentaxes', ax(4));
    plot(t_estimate, (plot_data.y_max_est(t_estimate,1)) * 1e6, 'LineWidth', 2, 'Color', plot_color, 'LineStyle', '-');
    hold on
    plot((plot_data.y_sample(:,1)) * 1e6, 'LineWidth', 2, 'Color', plot_color, 'LineStyle', ':')


end

for c1 = 1:5
    set(f, 'currentaxes', ax(c1));
    set(gca, 'FontSize', 14);
       
    if c1 < 5
        patch([1 10 10 1], [0 0 60 60], .5*ones(1,3), 'FaceAlpha', .3, 'EdgeColor', 'none')
        xlim([1 30])
    end
    
    box off
    if c1 < 4
        xticklabels({})
    end
end

legend([h1(2) h2(2) h1(1) h2(1)], {'UCB(0.4) Estimated Optimal', 'UCB(0.4) Sample','UCB(0.01) Estimated Optimal', 'UCB(0.01) Sample'}, ...
    'box', 'off', 'position', [0.2910 0.8844 0.2070 0.0936])

set(f, 'currentaxes', ax(1));
ylabel('Amplitude')
ylim([0 60])

set(f, 'currentaxes', ax(2));
ylabel('Frequency')
ylim([0 45])

set(f, 'currentaxes', ax(3));
ylabel('Pulse Width')
ylim([0 12])

set(f, 'currentaxes', ax(4));
ylabel('Gamma Power (mV^2/Hz)')
xlabel('Sample')
ylim([0 1])

set(f, 'currentaxes', ax(5));
ylabel('log_{10}[Gamma (mV^2/Hz)]')
%
set(f, 'currentaxes', ax(5))
paper_dir               = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';

experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.01/'];
optimization_data_dir   = [experiment_dir 'test_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

data_dir{1}             = [processed_data_dir 'EPI033_compet_result'];
load(data_dir{1})
hold on

hb = boxplot(log10(1e6*OF_sort_os(2,:)), [  2*ones(1,10) ones(1,30)]);

marker_style            = {'o', 's', 'd', '^'};

% OF_sort_os              = [OF_sort_os(11:40,:) OF_sort_os(1:10,:)
for c1 = 1:4

    if c1 == 1
        plot_color  = dark_2(4,:);
        x_center    = 2;
    else
        x_center    = 1;
        plot_color  = dark_2(3,:);
    end
    
    x_rand      = x_center + (rand(10,1)-.5)*.2;
    
    h(c1) = scatter(x_rand, log10(1e6*OF_sort_os(2,(c1-1)*10+1:c1*10)), 200, 'MarkerEdgeColor',...
        'k', 'MarkerFaceColor', plot_color, 'MarkerFaceAlpha', .5, 'marker', marker_style{c1});
end

set(hb(:,1), 'MarkerEdgeColor', dark_2(3,:));
set(hb(:,1), 'color', dark_2(3,:));
set(hb(:,2), 'MarkerEdgeColor', dark_2(4,:));
set(hb(:,2), 'color', dark_2(4,:));
set(hb, 'LineWidth', 2);
box off
plot([1 2], -.5*[1 1], 'k')
plot(1.5, -.48, 'k*','MarkerSize',10) 
legend([h(2:4) h(1)], {'UCB(0.4) Trial 1','UCB(0.4) Trial 2','UCB(0.4) Trial 3', 'UCB(0.01)'}, ...
    'position', [0.8320 0.8664 0.1300 0.0936], 'box', 'off')
xticks([1 2])
xticklabels({'UCB(0.4)', 'UCB(0.01)'})
ylim([-1.3 -.4])


y_coords        = [.88 ];
x_coords        = [.045 .5];
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

print('configuration_in_vivo', '-dpng', '-r500')
