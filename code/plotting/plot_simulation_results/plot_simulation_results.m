function plot_simulation_results(results_dir)
close all;

figure( 'Position',  [190, 100, 1100, 1000])
dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;
    
subject_1a  = 1;
d           = dir(results_dir);
data        = [];

x_grid      = [.05 .375 .7]+.03;
y_grid      = [.75 .525 .42 .315 .075];
width       = .25;
height      = [.2 .1 .15];

for c1 = 1:size(d,1)
    f_name = d(c1).name;
    
    if d(c1).isdir
        continue;
    end
    
    results_path    = [results_dir filesep f_name];
    data            = [data load(results_path)];
end

e_dist              = data(subject_1a).e_dist;
e_trace             = data(subject_1a).e_trace;
e_sample            = data(subject_1a).e_sample;
stim_params         = data(subject_1a).stim_params;
optimal_est         = data(subject_1a).optimal_est;
optimal_gt          = data(subject_1a).optimal_ground_truth;

for c1 = 1:size(data,2)
    best_param(c1,:) = get_best_params(data(c1).e_dist);
end

%%%
ax              = plot_detailed(e_dist(1:12,:), best_param(:,1), dark_2(1,:));
ylabel('Final Error')
title('SPSA')
xticklabels({'a=1, c=1', 'a=100, c=1', 'a=500, c=1', 'a=1000, c=1'...
    , 'a=1, c=50', 'a=100, c=50', 'a=500, c=50', 'a=1000, c=50'...
    , 'a=1, c=200', 'a=100, c=200', 'a=500, c=200', 'a=1000, c=200'})
xtickangle(45)
ax(1).Position  = [x_grid(1) y_grid(1) width(1) height(1)];


ax(2) = plot_detailed(e_dist(13:24,:), best_param(:,2) - 12, dark_2(2,:));
ylabel(' ')
title('CEM')
xtickangle(45)
xticklabels({'N=4, LR=1', 'N=5, LR=1', 'N=10, LR=1', 'N=20, LR=1'...
    , 'N=4, LR=0.8', 'N=5, LR=0.8', 'N=10, LR=0.8', 'N=20, LR=0.8'...
    , 'N=4, LR=0.6', 'N=5, LR=0.6', 'N=10, LR=0.6', 'N=20, LR=0.6'})
xtickangle(45)
yticklabels({})
ax(2).Position = [x_grid(2) y_grid(1) width(1) height(1)];

%
ax(3) = plot_detailed(e_dist(25:36,:), best_param(:,3) - 24, dark_2(3,:));
ylabel(' ')
title('BaO')
xticklabels({'PI(0.01)', 'PI(0.05)', 'PI(0.1)', 'PI(1.0)'...
    , 'EI(0.005)', 'EI(0.01)', 'EI(0.1)', 'EI(0.3)'...
    , 'UCB(0.1)', 'UCB(0.2)', 'UCB(0.4)', '    UCB(0.01)'})
xtickangle(45)
yticklabels({})
ax(3).Position = [x_grid(3) y_grid(1) width(1) height(1)];


%%% Representative traces 
figure_range        = [ 10; 13; 16];
ax([4 5 6])         = plot_representative_trace(optimal_est(best_param(subject_1a,1)), ...
    stim_params(best_param(subject_1a,1)), optimal_gt, figure_range, dark_2(1,:));
ax(4).Position      = [x_grid(1) y_grid(2) width(1) height(2)];
ax(4).YLabel.String = 'Amplitude';
ax(5).Position      = [x_grid(1) y_grid(3) width(1) height(2)];
ax(5).YLabel.String = 'Frequency';
ax(6).Position      = [x_grid(1) y_grid(4) width(1) height(2)];
ax(6).YLabel.String = 'Pulse width';

figure_range        = [11; 14; 17];
ax([7 8 9])         = plot_representative_trace(optimal_est(best_param(subject_1a,2)), ...
    stim_params(best_param(subject_1a,2)), optimal_gt, figure_range, dark_2(2,:))
ax(7).Position      = [x_grid(2) y_grid(2) width(1) height(2)];
ax(8).Position      = [x_grid(2) y_grid(3) width(1) height(2)];
ax(9).Position      = [x_grid(2) y_grid(4) width(1) height(2)];

figure_range = [12; 15; 18];
ax([10 11 12])      = plot_representative_trace(optimal_est(best_param(subject_1a,3)), ...
    stim_params(best_param(subject_1a,3)), optimal_gt, figure_range, dark_2(3,:))
ax(10).Position      = [x_grid(3) y_grid(2) width(1) height(2)];
ax(11).Position      = [x_grid(3) y_grid(3) width(1) height(2)];
ax(12).Position      = [x_grid(3) y_grid(4) width(1) height(2)];

%%% Performance of all trials for best hyperparamters 
ax(13)              = plot_trial_bounds(e_trace, e_sample, best_param(subject_1a,:));
ax(13).Position     = [x_grid(1) y_grid(5) width(1) height(3)];

%%% Summary statistics
best_dist = [];
best_conv = [];
for c1 = 1:size(data,2)
    ed          = data(c1).e_dist;
    ec          = data(c1).e_converge;
    best_param  = get_best_params(ed);

    best_dist   = [best_dist; ed(best_param,:)];
    best_conv   = [best_conv; ec(best_param,:)];
end

ax(14) = plot_summary(best_dist);
hold(ax(14),'on')
ax(14).Position     = [x_grid(2) y_grid(5) width(1) height(3)];

dim = [.2 .5 .3 .3];
str = 'Straight Line Plot from 1 to 10';

%
plot([.75 1],[.11 .11], 'k')
plot(1.75/2, .13, 'k*')

plot([1 1.25],[.15 .15], 'k')
plot(2.25/2 + [-.1 0 .1], .17*[1 1 1], 'k*')

plot([.75 1.25],[.19 .19], 'k')
plot(2/2 + [-.05 .05], .21*[1 1], 'k*')

%
plot([.75 1]+1,[.11 .11]+.2, 'k')
plot(1.75/2+1  + [-.05 .05], (.13+.2)*[1 1], 'k*')

plot([1 1.25]+1,[.15 .15]+.2, 'k')
plot(2.25/2+1, .17+.2, 'k*')

plot([.75 1.25]+1,[.19 .19]+.2, 'k')
plot(2/2+1  + [-.05 .05], (.21+.2)*[1 1], 'k*')

%
plot([.75 1]+2,[.11 .11]+.11, 'k')
plot(1.75/2+2, .13+.11, 'k*')

plot([1 1.25]+2,[.15 .15]+.11, 'k')
plot(2.25/2+2, .17+.11, 'k*')

plot([.75 1.25]+2,[.19 .19]+.11, 'k')
plot(2/2+2, .21+.11, 'k*')


ylabel('Final Error');


ax(15)              = plot_summary(best_conv);
hold(ax(15),'on')
ax(15).Position     = [x_grid(3) y_grid(5) width(1) height(3)];

% plot([.75 1],[0 0]-.15, 'k')
% plot(1.75/2, -.18, 'k*')

plot([1 1.25],[0. 0.]-.21, 'k')
plot(2.25/2 + [-.05 .05], -.24*[1 1], 'k*')

plot([.75 1.25],[0. 0.]-.27, 'k')
plot(1 + [-.05 .05], -.30*[1 1] , 'k*')

%
plot([.75 1]+1,[0 0]-.35, 'k')
plot(1.75/2+1 + [-.1 0 .1], -.39*[1 1 1], 'k*')

% plot([1 1.25]+1,[0 0]-.41, 'k')
% plot(2.25/2+1, -.44, 'k*')

% plot([.75 1.25]+1,[0 0]-.47, 'k')
% plot(1+1, -.50, 'k*')

%
plot([.75 1]+2,[.0 .0]-.45, 'k')
plot(1.75/2+2, -.48, 'k*')

plot([1 1.25]+2,[0 0]-.51, 'k')
plot(2.25/2+2, -.54, 'k*')

plot([.75 1.25]+2,[0 0]-.57, 'k')
plot(1+2 + [-.05 .05], -.60*[1 1], 'k*')
ylim([-.62 0])

ylabel('Convergence Rate')

y_coords        = [.88  .57 .17 ];
x_coords        = x_grid - .03;
sublabel_size   = 24;

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','E','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(3) y_coords(1) .1 .1],'String','F','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

% annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','E','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(3) y_coords(2) .1 .1],'String','F','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% 
% annotation('textbox',[x_coords(1) y_coords(3) .1 .1],'String','G','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(2) y_coords(3) .1 .1],'String','H','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(3) y_coords(3) .1 .1],'String','I','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

print( 'simulation_results_R3.eps','-depsc')

end 


function ax = plot_representative_trace(optimal_est, stim_params, optimal_gt, figure_range, dark_2)

ax(1)           = axes('Position', [.1 .1 .1 .1]);
hold on
optimal_idx     = optimal_est{1}(:,1);
optimal_trace   = optimal_est{1}(:,2);
sample_trace    = stim_params{1}(:,1);

plot(optimal_idx, optimal_trace, 'color', dark_2, 'linewidth', 2)
plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
plot([1 size(sample_trace,1)], [optimal_gt(1) optimal_gt(1)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

xticks([])
xticklabels({})
ylim([10 55])
set(gca,'FontSize', 16)

%%%%
ax(2)           = axes('Position', [.1 .1 .1 .1]);
hold on
optimal_trace   = optimal_est{1}(:,3);
sample_trace    = stim_params{1}(:,2);

plot(optimal_idx,optimal_trace, 'color', dark_2, 'linewidth', 2)
plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
plot([1 size(sample_trace,1)], [optimal_gt(2) optimal_gt(2)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

ylim([0 45])
xticks([])
xticklabels({})
set(gca,'FontSize', 16)

%%%%
ax(3)           = axes('Position', [.1 .1 .1 .1]);
hold on
optimal_trace   = optimal_est{1}(:,4);
sample_trace    = stim_params{1}(:,3);

plot(optimal_idx,optimal_trace, 'color', dark_2, 'linewidth', 2)
plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
plot([1 size(sample_trace,1)], [optimal_gt(3) optimal_gt(3)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

ylim([0 11])
set(gca,'FontSize', 16)
xlabel('Sample');

end


function ax = plot_summary(best_data)
ax(1)      	= axes('Position', [.1 .1 .1 .1]);

dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;
    
e_dist_mean = mean(best_data,2);
e_dist_std  = std(best_data, [], 2);

Y           = reshape(e_dist_mean', 3, 3);
err_Y       = reshape(e_dist_std', 3, 3);
err_Y       = err_Y / sqrt(30);

[h, he]     = barwitherr(err_Y',Y');

set(h(1),'FaceColor',dark_2(1,:))
set(h(2),'FaceColor',dark_2(2,:))
set(h(3),'FaceColor',dark_2(3,:))

set(h(1),'EdgeColor',dark_2(1,:))
set(h(2),'EdgeColor',dark_2(2,:))
set(h(3),'EdgeColor',dark_2(3,:))

set(h, 'FaceAlpha', .25)
set(h,'LineWidth',3)
set(he,'LineWidth',3)

xticklabels({'R1', 'R2', 'R3'})
set(gca, 'FontSize', 16)
box off
end


function best_param = get_best_params(ed)
score_50        = mean(ed,2);
score_mat       = reshape(score_50, 12, []);
[~, min_param]  = min(score_mat);

best_param      = min_param + (0:12:24);
end


function ax = plot_detailed(e_dist, best_param, dark_2)
ax          = axes('Position', [.1 .1 .1 .1]);
h           = boxplot(e_dist');

set(h, 'color', .7*ones(1,3));
set(h, 'MarkerEdgeColor', .7*ones(1,3));
set(h, 'LineWidth', 2);
set(h(:,best_param(:,1)), 'color', dark_2);

hold on

e_dist_mean = mean(e_dist,2);
plot(e_dist_mean, 'x', 'markersize',15, 'color', .7*ones(1,3),'LineWidth',4)
plot(best_param(:,1), e_dist_mean(best_param(:,1)), 'x', 'markersize',15, 'color', dark_2(1,:),'LineWidth',4)

set(gca, 'FontSize', 16)
ylim([-.05 1])
box off

end


function ax = plot_trial_bounds(e_trace, e_sample, param)

dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;
ax(1)           = axes('Position', [.1 .1 .1 .1]);

for c1 = 1:size(param,2)
    
    clear traces samples
    for c2 = 1:29
        traces(c2,:) = e_trace{param(c1),c2}*-1;
    end
    
    samples = e_sample{param(c1),1};
    
    p25     = prctile(traces,25);
    p50     = prctile(traces,50);
    p75     = prctile(traces,75);
    
    patch_plot(samples, p50', p25', p75', dark_2(c1,:));
    hold on
end

set(gca, 'FontSize', 16)
xlabel('Samples')
ylabel('Error')
legend({'SPSA', '', 'CEM', '', 'BaO', ''}, 'Box', 'off')
box off
end