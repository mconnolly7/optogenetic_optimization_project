function plot_grid_search_1
data_table      = readtable('data/memory_data/1D_amplitude/data_merged_2.xlsx');
test_idx        = strcmp(data_table.phase, 'B');
data_table      = data_table(test_idx,:);

voltage         = data_table.voltage;
condition       = condition_onehot(data_table.condition);
experiment      = experiment_onehot(data_table.experiment);

% pre all
idx             = experiment == 1;
data_pre        = data_table.DS(idx);
pre_m           = nanmean(data_pre);
pre_s           = nanstd(data_pre);
pre_se          = pre_s / sqrt(sum(idx));

% intervention-sham
idx             = experiment == 2 & condition == 1;
data_int_sham   = data_table.DS(idx);
int_sham_m      = nanmean(data_int_sham);
int_sham_s      = nanstd(data_int_sham);
int_sham_se     = int_sham_s / sqrt(sum(idx));

% intervention-2V
idx             = experiment == 2 & condition == 2 & voltage == 2;
data_int_2v     = data_table.DS(idx);
int_2v_m        = nanmean(data_int_2v);
int_2v_s        = nanstd(data_int_2v);
int_2v_se       = int_2v_s / sqrt(sum(idx));

% intervention-4V
idx             = experiment == 2 & condition == 2 & voltage == 4;
data_int_4v     = data_table.DS(idx);
int_4v_m        = nanmean(data_int_4v);
int_4v_s        = nanstd(data_int_4v);
int_4v_se       = int_4v_s / sqrt(sum(idx));

% post-sham
idx_post_sham             = experiment == 3 & condition == 1;
data_post_sham  = data_table.DS(idx_post_sham);
post_sham_m     = nanmean(data_post_sham);
post_sham_s     = nanstd(data_post_sham);
post_sham_se    = post_sham_s / sqrt(sum(idx_post_sham));

% post-2V
idx_post_2v             = experiment == 3 & condition == 2 & voltage == 2;
data_post_2v    = data_table.DS(idx_post_2v);
post_2v_m       = nanmean(data_post_2v);
post_2v_s       = nanstd(data_post_2v);
post_2v_se      = post_2v_s / sqrt(sum(idx_post_2v));

% post-4V
idx_post_4v  = experiment == 3 & condition == 2 & voltage == 4;
data_post_4v    = data_table.DS(idx_post_4v);
post_4v_m       = nanmean(data_post_4v);
post_4v_s       = nanstd(data_post_4v);
post_4v_se      = post_4v_s / sqrt(sum(idx_post_4v));

l_sham          = [pre_m int_sham_m post_sham_m];
se_sham         = [pre_se int_sham_se post_sham_se];

l_2v            = [pre_m int_2v_m post_2v_m];
se_2v           = [pre_se int_2v_se post_2v_se];

l_4v            = [pre_m int_4v_m post_4v_m];
se_4v           = [pre_se int_4v_se post_4v_se];

close all;
hold on

% 2V 
xf = [0 .95 1.95];
errorbar(xf,l_2v, se_2v, 'LineWidth', 2)

% 2V 
xf = [0 1.05 2.05];
errorbar(xf,l_4v, se_4v, 'LineWidth', 2)

% sham 
xf = [0 1 2];
errorbar(xf,l_sham, se_sham, 'LineWidth', 2)

ylabel('DS')
legend({'2V', '4V', 'Sham'})
xticks([0 1 2])
xticklabels({'Pre', 'Intervention', 'Post'})
set(gca, 'FontSize', 16)

ylim([-1 1])
xlim([-.1 2.2])
end

function experiment_code = experiment_onehot(experiment)

for c1 = 1:size(experiment,1)
    
    switch(experiment{c1})
        case 'pre'
            experiment_code(c1,1) = 1;
        case 'intervention'
            experiment_code(c1,1) = 2;
        case 'post'
            experiment_code(c1,1) = 3;
            
    end
end
end
function condition_code = condition_onehot(condition)

for c1 = 1:size(condition,1)
    
    switch(condition{c1})
        case 'sham'
            condition_code(c1,1) = 1;
        case 'stim'
            condition_code(c1,1) = 2;        
    end
    
end
end



