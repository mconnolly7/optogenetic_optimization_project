function plot_error_convergence_evoked_potential(results_dir)
close all;

dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;
    
subject_1a  = 1;
d           = dir(results_dir);
data        = [];

for c1 = 1:size(d,1)
    f_name = d(c1).name;
    
    if d(c1).isdir
        continue;
    end
    
    results_path    = [results_dir filesep f_name];
    data            = [data load(results_path)];
end

e_dist              = data(subject_1a).e_dist;
e_trace             = data(subject_1a).e_trace;
e_sample            = data(subject_1a).e_sample;
stim_params         = data(subject_1a).stim_params;
optimal_est         = data(subject_1a).optimal_est;
optimal_gt          = data(subject_1a).optimal_ground_truth;

for c1 = 1:size(data,2)
    best_param(c1,:) = get_best_params(data(c1).e_dist);
end

%%%
subplot(7,12,[1:4 13:16]);
plot_detailed(e_dist(1:12,:), best_param(:,1), dark_2(1,:))
ylabel('Final Error')
title('SPSA')

subplot(7, 12,[5:8 17:20])
plot_detailed(e_dist(13:24,:), best_param(:,2) - 12, dark_2(2,:))
ylabel(' ')
title('CEM')

subplot(7, 12,[9:12 21:24])
plot_detailed(e_dist(25:36,:), best_param(:,3) -24, dark_2(3,:))
ylabel(' ')
title('BaO')

%%% Representative traces 
figure_range = [25:28; 37:40; 49:52];
plot_representative_trace(optimal_est(best_param(subject_1a,1)), ...
    stim_params(best_param(subject_1a,1)), optimal_gt, figure_range, dark_2(1,:))

subplot(7,12, figure_range(1,:))
ylabel('Channel');
subplot(7,12, figure_range(2,:))
ylabel('Amplitude (mA)');
subplot(7,12, figure_range(3,:))
ylabel('Pulse width');

figure_range = [29:32; 41:44; 53:56];
plot_representative_trace(optimal_est(best_param(subject_1a,2)), ...
    stim_params(best_param(subject_1a,2)), optimal_gt, figure_range, dark_2(2,:))

figure_range = [33:36; 45:48; 57:60];
plot_representative_trace(optimal_est(best_param(subject_1a,3)), ...
    stim_params(best_param(subject_1a,3)), optimal_gt, figure_range, dark_2(3,:))

%%% Performance of all trials for best hyperparamters 
subplot(7,12,[61:66 73:78])
plot_trial_bounds(e_trace, e_sample, best_param(subject_1a,:))

%%% Summary statistics
best_dist = [];
best_conv = [];
for c1 = 1:size(data,2)
    ed          = data(c1).e_dist;
    ec          = data(c1).e_converge;
    best_param  = get_best_params(ed);

    best_dist   = [best_dist; ed(best_param,:)];
    best_conv   = [best_conv; ec(best_param,:)];
end

subplot(7,12,[67:69 79:81])
plot_summary(best_dist)
ylabel('Final Error');


subplot(7,12,[70:72 82:84])
plot_summary(best_conv)
ylabel('Convergence Rate')
end 


function plot_representative_trace(optimal_est, stim_params, optimal_gt, figure_range, dark_2)

subplot(7,12, figure_range(1,:))
hold on
optimal_idx     = optimal_est{1}(:,1);
optimal_trace   = optimal_est{1}(:,2);
sample_trace    = stim_params{1}(:,1);

plot(optimal_idx, optimal_trace, 'color', dark_2, 'linewidth', 2)
plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
plot([1 size(sample_trace,1)], [optimal_gt(1) optimal_gt(1)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

xticks([])
xticklabels({})
ylim([0 6])
set(gca,'FontSize', 12)

%%%%
subplot(7,12, figure_range(2,:))
hold on
optimal_trace   = optimal_est{1}(:,3);
sample_trace    = stim_params{1}(:,2);

plot(optimal_idx,optimal_trace, 'color', dark_2, 'linewidth', 2)
plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
plot([1 size(sample_trace,1)], [optimal_gt(2) optimal_gt(2)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

ylim([0 6])
xticks([])
xticklabels({})
set(gca,'FontSize', 12)

%%%%
subplot(7,12, figure_range(3,:))
hold on
optimal_trace   = optimal_est{1}(:,4);
sample_trace    = stim_params{1}(:,3);

% plot(optimal_idx,optimal_trace, 'color', dark_2, 'linewidth', 2)
% plot(sample_trace, 'color', dark_2, 'linewidth', 2, 'linestyle', ':')
% plot([1 size(sample_trace,1)], [optimal_gt(2) optimal_gt(2)], 'color', 'k', 'linewidth', 2, 'linestyle', '--')

% ylim([0 11])
set(gca,'FontSize', 12)
xlabel('Sample');

end


function plot_summary(best_data)

dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;
    
    
% best_data = [];
% for c1 = 1:size(data,2)
%     ed          = data(c1).e_dist;
%     best_param  = get_best_params(ed);
% 
%     best_data   = [best_data; ed(best_param,:)];
%     
% end

e_dist_mean = mean(best_data,2);
e_dist_std  = std(best_data, [], 2);

Y           = reshape(e_dist_mean', 3, 3);
err_Y       = reshape(e_dist_std', 3, 3);
err_Y       = err_Y / sqrt(30);

[h, he] = barwitherr(err_Y',Y');

set(h(1),'FaceColor',dark_2(1,:))
set(h(2),'FaceColor',dark_2(2,:))
set(h(3),'FaceColor',dark_2(3,:))

set(h(1),'EdgeColor',dark_2(1,:))
set(h(2),'EdgeColor',dark_2(2,:))
set(h(3),'EdgeColor',dark_2(3,:))

set(h, 'FaceAlpha', .25)
set(h,'LineWidth',3)
set(he,'LineWidth',3)

xticklabels({'R1', 'R2', 'R3'})
set(gca, 'FontSize', 12)

end


function best_param = get_best_params(ed)
score_50        = mean(ed,2);
score_mat       = reshape(score_50, 12, []);
[~, min_param]  = min(score_mat);

best_param      = min_param + (0:12:24);
end


function plot_detailed(e_dist, best_param, dark_2, h)
    
h           = boxplot(e_dist');

set(h, 'color', .7*ones(1,3));
set(h, 'MarkerEdgeColor', .7*ones(1,3));
set(h, 'LineWidth', 2);
set(h(:,best_param(:,1)), 'color', dark_2);

hold on

e_dist_mean = mean(e_dist,2);
plot(e_dist_mean, 'x', 'markersize',15, 'color', .7*ones(1,3),'LineWidth',4)
plot(best_param(:,1), e_dist_mean(best_param(:,1)), 'x', 'markersize',15, 'color', dark_2(1,:),'LineWidth',4)

set(gca, 'FontSize', 12)
% xticklabels([])
ylim([-.05 1])

end


function plot_trial_bounds(e_trace, e_sample, param)

dark_2  = [27,158,119
        217,95,2
        117,112,179]/255;

for c1 = 1:size(param,2)
    
    clear traces samples
    for c2 = 1:29
        traces(c2,:) = e_trace{param(c1),c2}*-1;
    end
    
    samples = e_sample{param(c1),1};
    
    p25     = prctile(traces,25);
    p50     = prctile(traces,50);
    p75     = prctile(traces,75);
    
    patch_plot(samples, p50', p25', p75', dark_2(c1,:));
    hold on
end

set(gca, 'FontSize', 12)
xlabel('Samples')
ylabel('Error')
legend({'SPSA', '', 'CEM', '', 'BaO', ''})

end