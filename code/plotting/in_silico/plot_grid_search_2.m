%%
data_table  = readtable('NOR_grid_search.csv');
data_table  = data_table(data_table.ID == 73,:);
stim_params = [data_table.Voltage data_table.Frequency];

stim_u      = unique(stim_params, 'rows');

for c1 = 1:size(stim_u,1)
    stim_idx = stim_params(:,1) == stim_u(c1,1) & stim_params(:,2) == stim_u(c1,2);
    
    d_score     = data_table.DS(stim_idx);
    m(c1)       = mean(d_score);
    s(c1)       = std(d_score);
    se(c1)      = s(c1)/sqrt(size(d_score,1));
    
end

%%
close all;
hold on

% 1V
idx_v       = stim_u(:,1) == 1 | stim_u(:,1) == 0 ;
xf          = stim_u(idx_v,2);
xf(2:end)   = xf(2:end) + .15;
m1          = m(idx_v);
se1         = se(idx_v);
errorbar(xf,m1, se1, 'LineWidth', 2)

% 3V
idx_v       = stim_u(:,1) == 3 | stim_u(:,1) == 0 ;
xf          = stim_u(idx_v,2);
xf(2:end)   = xf(2:end) - .15;
m1          = m(idx_v);
se1         = se(idx_v);
errorbar(xf,m1, se1, 'LineWidth', 2)

xlabel('Frequency')
ylabel('DS')
legend({'1V', '3V'})
set(gca, 'FontSize', 16)
ylim([-1 1])




