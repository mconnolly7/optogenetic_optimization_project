clear; close all;
figure( 'Position',  [100, 100, 1500, 500])

control_path    = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/data/Control_animal_feat.mat';
gamma_path      = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_2D/R1_gamma.mat';

load(control_path)

unique_subjects = unique(stimtable(:,4));

for c1 = 1:size(unique_subjects,1)
    subplot(1,3,c1)
    subject_idx = stimtable(:,4) == unique_subjects(c1);
    
    X_data = stimtable(subject_idx,1:2);
    Y_data = stimall(subject_idx)*1e6;
    
    gp_model = gp_object();
    gp_model.initialize_data(X_data, Y_data)
    gp_model.plot_mean
end

subplot(1,3,3)

load('optogenetic_optimization_project/data/in_silico_data/processed_data/laxpati_2014/hSynChR2_PSD_COH_W20_Ch1-16_1-100Hz_1Hzbin.mat')

subject_idx     = stimtable(:,4) == 1;
X_data          = stimtable(subject_idx,[1 2]);
gamma_range     = squeeze(PSDall(subject_idx,2,:,33:50));
gamma_power     = sum(gamma_range,3);
gamma_total     = sum(gamma_power,2);
valid_idx       = ~isnan(gamma_total);


gp_model.initialize_data(X_data(valid_idx,:),gamma_total(valid_idx)*1e6);
gp_model.plot_mean
max_gamma       = 0.05;

for c1 = 1:3
    subplot(1,3,c1)
    zlim([0 max_gamma])
    set(gca,'FontSize', 14)
    xlim([10 50])
    ylim([5 42])
end

subplot(1,3,1)
xlabel('Amplitude (mW/mm^2)')
ylabel('Frequency (Hz)')
zlabel('Gamma Power (mV^2/Hz)');
title('Subject C1: hSyn-eYFP')

subplot(1,3,2)
title('Subject C2: hSyn-eYFP')

subplot(1,3,3)
title('Subject R1: hSyn-ChR2(H134)-eYFP')
    colormap(viridis)

print('control_figures', '-dpng', '-r500')


