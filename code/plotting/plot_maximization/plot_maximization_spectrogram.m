params.Fs       = 2000;
params.tapers   = [3 5];
params.fpass    = [4 55];

subject         = 'EPI032';
exp_date        = '2018_11_15';
paper_dir       = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
raw_dir         = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/optimization_data/raw_data/'];
subject_dir     = [raw_dir subject filesep];
exp_dir         = [subject_dir subject '_' exp_date filesep];

experiment_dirs = dir([exp_dir 'E*']);
for c2 = 1:size(experiment_dirs,1);
    
    data_dir = [exp_dir experiment_dirs(c2).name '/'];
    
    d   = dir([exp_dir 'Resampl*']);

    S_all           = [];

    for c1 = 1:size(d,1)
        file_name = d(c1).name;
        file_path = [exp_dir file_name];

        load(file_path, 'data');

        [S t f] = mtspecgramc(data(1,:), [1 1], params);
        S_all = [S_all; S];
    %     S_all = [S_all; S ];
    end

    t_all = 1:1:size(S_all,1);
    
    plot_matrix(S_all*1e6, t_all, f);
end

colorbar off