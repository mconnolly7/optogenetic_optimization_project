%% Organize data
clear; clc;
paper_dir               = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.01/'];
optimization_data_dir   = [experiment_dir 'optimization_data/'];
raw_data_dir            = [optimization_data_dir 'raw_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

data_dir            = 'optogenetic_optimization_project/data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/';
subject_dirs        = {'EPI033-GP' 'EPI032-GP'};


for c1 = 1:size(subject_dirs,2)
    subject_path    = [processed_data_dir subject_dirs{c1}];
    d               = dir(subject_path);
    
    for c2 = 1:size(d,1)        
        experiment_dir = d(c2).name;
        
        if ~strcmp(experiment_dir(1),'.') && d(c2).isdir
           experiment_path = [subject_path filesep experiment_dir];
          
           load([experiment_path filesep 'gp_model.mat'])
           
           [x_max_est, y_max_est]       = get_optimization_trajectory(gp_temp);
           
           plot_data(c1,c2).x_sample    = gp_temp.x_data;
           plot_data(c1,c2).y_sample    = gp_temp.y_data;
           
           plot_data(c1,c2).x_max_est   = x_max_est;
           plot_data(c1,c2).y_max_est   = y_max_est;
           
        end
    end
end

%%
results_dir   = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.01/';
figure_3_data = plot_data';
save([results_dir 'figure_S3_data.mat'], 'figure_3_data');


%% Plot figure 3
% close all; clc;

f               = figure( 'Position',  [100, 100, 1000, 900]);

x_grid          = [.07 .53];
y_grid_1        = [.79 .62 .45 .28 .05];
y_grid_2        = [.79 .62 .45 .28 .05];

width           = .4;
height          = [.16] ;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid_2(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(2)  y_grid_2(2) width(1) height(1)]);
ax(5)        	= axes('Position', [x_grid(1)  y_grid_1(3) width(1) height(1)]);
ax(6)        	= axes('Position', [x_grid(2)  y_grid_2(3) width(1) height(1)]);
ax(7)        	= axes('Position', [x_grid(1)  y_grid_1(4) width(1) height(1)]);
ax(8)        	= axes('Position', [x_grid(2)  y_grid_2(4) width(1) height(1)]);
ax(9)        	= axes('Position', [x_grid(1)  y_grid_1(5) width(1) height(1)]);
ax(10)        	= axes('Position', [x_grid(2)  y_grid_2(5) width(1) height(1)]);

hold(ax(1),'on');hold(ax(3),'on');hold(ax(5),'on');hold(ax(7),'on');hold(ax(9),'on');
hold(ax(2),'on');hold(ax(4),'on');hold(ax(6),'on');hold(ax(8),'on');hold(ax(10),'on');

results_dir   = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.4/';
load([results_dir 'figure_3_data.mat'], 'figure_3_data');

p = [215,25,28
    43,131,186
    171,217,233]/255;

font_size = 12;
color_p = [215,25,28
253,174,97
171,221,164
43,131,186]/255;

intensity_map       = [43.8782, -169.9840
                    48.4936, -184.5139];
results_dir         = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.4/';

load([results_dir 'figure_3_data.mat']);
t_estimate = 10:30;

figure_3_data = [figure_3_data(:,2) figure_3_data(:,1)];

for c1 = 1:size(figure_3_data,2)
    offset = c1-1;
    for c2 = 1:size(figure_3_data,1)
        plot_data = figure_3_data(c2,c1);
        
        if isempty(plot_data.x_max_est)
            continue;
        end
        
        set(f, 'currentaxes', ax(1+offset));
        plot(t_estimate, plot_data.x_max_est(t_estimate,1) * intensity_map(c1,1) + intensity_map(c1,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,1) * intensity_map(c1,1) + intensity_map(c1,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        set(f, 'currentaxes', ax(3+offset));
        plot(t_estimate,plot_data.x_max_est(t_estimate,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        set(f, 'currentaxes', ax(5+offset));
        plot(t_estimate, plot_data.x_max_est(t_estimate,3), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,3), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        set(f, 'currentaxes', ax(7+offset));
        plot(t_estimate, (plot_data.y_max_est(t_estimate,1)) * 1e6, 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot((plot_data.y_sample(:,1)) * 1e6, 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')

    end
end

for c1 = 1:8
    set(f, 'currentaxes', ax(c1));
    box off
    patch([1 10 10 1], [0 0 60 60], .5*ones(1,3), 'FaceAlpha', .3, 'EdgeColor', 'none')
end

set(f, 'currentaxes', ax(1));
ylabel('Amplitude (mW/mm^2)')
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([9 60])
xticklabels({})
title('Subject R4', 'FontSize', 20)
legend({'Estimated Optimal Parameter', 'Sampled Parameter'}, 'position',[0.320 0.93 0.1840 0.0361], 'box', 'off')

set(f, 'currentaxes', ax(2));
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([9 60])
xticklabels({})
yticklabels({})
title('Subject R5', 'FontSize', 20)

set(f, 'currentaxes', ax(3));
ylabel('Pulse Frequency (Hz)')
set(gca, 'Fontsize', font_size)
xticklabels({})
xlim([1 30])
ylim([0 50])

set(f, 'currentaxes', ax(4));
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 50])
xticklabels({})
yticklabels({})

set(f, 'currentaxes', ax(5));
ylabel('Pulse Width (ms)')
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 10])
xticklabels({})

set(f, 'currentaxes', ax(6));
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 10])
xticklabels({})
yticklabels({})

set(f, 'currentaxes', ax(7));
xlim([1 30])
ylim([0 1])
ylabel('Gamma Power (mV^2/Hz)')
xticklabels({})
xlabel('Samples')
set(gca, 'Fontsize', font_size)

set(f, 'currentaxes', ax(8));
set(gca, 'Fontsize', font_size)
xlabel('Samples')
xlim([1 30])
ylim([0 1])
yticklabels({})
xticklabels({})

set(f, 'currentaxes', ax(9));
plot(t_estimate, figure_3_data(4,1).y_max_est(t_estimate,1)*1e6, 'LineWidth', 2, 'Color', p(1,:), 'LineStyle', '-');
hold on
plot(figure_3_data(4,1).y_sample(:,1)*1e6, 'LineWidth', 2, 'Color', p(1,:), 'LineStyle', ':')
plot(t_estimate, figure_3_data(5,1).y_max_est(t_estimate,1)*1e6, 'LineWidth', 2, 'Color', p(1,:), 'LineStyle', '-');
plot(figure_3_data(5,1).y_sample(:,1)*1e6, 'LineWidth', 2, 'Color', p(1,:), 'LineStyle', ':')

patch([1 10 10 1], [0 0 60 60], .5*ones(1,3), 'FaceAlpha', .3, 'EdgeColor', 'none')
ylabel('Gamma Power (mV^2/Hz)')

ylim([0 .06])
xlim([1 30])
set(gca, 'Fontsize', font_size)
xlabel('Samples')

box off

set(f, 'currentaxes', ax(10));
plot_maximization_spectrogram
title('')
xlabel('Samples')
ylabel('Frequency (Hz)')
xlim([1 600])
ylim([5 54])
set(gca, 'Fontsize', font_size)
xticks([1 81 181 281 381 481 581])
xticklabels({'1', '5', '10', '15', '20', '25', '30'})
colormap(magma)
sublabel_size   = 28;
c               = colorbar('Position',[ .94 y_grid_1(5) .01 height(1)]);
c.Label.String  = 'log[gamma power (mV^2/Hz)]';
c.Label.FontSize = font_size;
y_coords        = [.9 .16];
x_coords        = [.03 .49];

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

print( 'optimization_max.eps','-depsc')

