clear;clc;close all;
paper_dir           = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir      = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/'];
test_data_dir       = [experiment_dir 'test_data/'];
raw_data_dir        = [test_data_dir 'raw_data/'];
processed_data_dir  = [test_data_dir 'processed_data/'];

subject_data        = {'EPI033' 'EPI032'};

line_style          = {'--', ':','-'};
color_p = [215,25,28
    43,131,186
    171,217,233]/255;

params_vec = combvec([1 2], [1 2 3])';

for param_idx = 3%1:size(params_vec,1)
    figure( 'Position',  [100, 100, 1000, 900])
    subplot(2,2,1)
    for c2 = 1:size(subject_data,2)
        load([processed_data_dir subject_data{c2} '_compet_result']);
        of_data = OF_sort_os(:,11:end);
        hold on

        for c1 = 1:3
            x_rand = c1 + (rand(30,1)-.5)*.2;
            p_idx(c2) = scatter(x_rand, log(of_data(c1,:)), 200, 'MarkerEdgeColor',...
                   'k', 'MarkerFaceColor', color_p(c2,:), 'MarkerFaceAlpha', .5);

            trial_mean(c1,:) = mean(of_data(:,10*(c1-1)+1:10*c1),2);
            plot([1 2 3], log(trial_mean(c1,:)),'color',color_p(c2,:), 'LineStyle', line_style{c1}, 'LineWidth', 3)

        end
    end

    xticks([1 2 3])
    xticklabels({'Sham', 'Optimal', 'Worst'});
    ylabel('log Gamma Power (V^2/Hz)')
    legend(p_idx, {'R4', 'R5'})
    set(gca, 'FontSize', 16)
    xlim([.5 3.5])

    subject_idx         = params_vec(param_idx,1);
    trial_idx           = params_vec(param_idx,2);
    channel             = 3;
    sampling_rate       = 2000;
    subject_data_dir    = [raw_data_dir subject_data{subject_idx} '/'];

    d = dir([subject_data_dir 'EPI*']);

    trial_data_path = [subject_data_dir d(trial_idx).name];

    d_sham          = dir([trial_data_path '/Sham/Re*']);
    d_optimal       = dir([trial_data_path '/Optimal/Re*']);
    d_suboptimal    = dir([trial_data_path '/Suboptimal/Re*']);

    params.tapers   = [3 5];
    params.Fs       = sampling_rate;
    params.fpass    = [1 100];

    % hold on
    for c1 = 1:size(d_sham,1)
       data_path            = [d_sham(c1).folder filesep d_sham(c1).name];
       load(data_path, 'data');
       data_channel         = data(channel,5*sampling_rate:10*sampling_rate);
       data_channel_norm    = data_channel - mean(data_channel);
       [S(1, c1,:), F]      = mtspectrumc(data_channel_norm, params);

       data_path            = [d_optimal(c1).folder filesep d_optimal(c1).name];
       load(data_path, 'data');
       data_channel         = data(channel,5*sampling_rate:10*sampling_rate);
       data_channel_norm    = data_channel - mean(data_channel);
       [S(2, c1,:), F]      = mtspectrumc(data_channel_norm, params);

       data_path            = [d_suboptimal(c1).folder filesep d_suboptimal(c1).name];
       load(data_path, 'data');
       data_channel         = data(channel,5*sampling_rate:10*sampling_rate);
       data_channel_norm    = data_channel - mean(data_channel);
       [S(3, c1,:), F]      = mtspectrumc(data_channel_norm, params);

    end

    S_mean  = squeeze(mean(log(S),2));
    S_std   = squeeze(std(log(S),[],2));
    S_se    = S_std / sqrt(10);
    p_color = [231,41,138
                102,166,30
                230,171,2]/255;

    subplot(2,2,2)

    for c1 = 1:3
        FF      = [F flip(F)]';
        SS      = [S_mean(c1,:) - S_std(c1,:) flip(S_mean(c1,:) + S_std(c1,:))]';

        hold on
        patch(FF, SS, p_color(c1,:), 'FaceAlpha', .5)
        hold on
        plot_idx(c1) = plot(F, S_mean(c1,:), 'color', p_color(c1,:), 'LineWidth', 2);
    end
    xlim([4 100])
    xlabel('Frequency (Hz)')
    ylabel('log [Gamma Power (V^2/Hz)]')
    set(gca, 'FontSize', 16)
    legend(plot_idx, {'Sham', 'Optimal', 'Suboptimal'})
    
    %%%
    subplot(2,2,4)

    for c1 = 1:3
        FF      = [F flip(F)]';
        SS      = [S_mean(c1,:) - S_std(c1,:) flip(S_mean(c1,:) + S_std(c1,:))]';

        hold on
        patch(FF, SS, p_color(c1,:), 'FaceAlpha', .5)
        hold on
        plot_ax(c1) = plot(F, S_mean(c1,:), 'color', p_color(c1,:), 'LineWidth', 2);
    end
    
    xlim([30 55])
    xlabel('Frequency (Hz)')
    ylabel('log [Gamma Power (V^2/Hz)]')
    set(gca, 'FontSize', 16)
    
    subplot(2,2,3)
    hold on
    sample_idx      = 5;
    t               = (1:15*sampling_rate)/sampling_rate;
    
    data_path       = [d_sham(sample_idx).folder filesep d_sham(sample_idx).name];
    load(data_path, 'data');
    data_seg        = data(channel,1:15*sampling_rate);
    data_band       = bandpass(data_seg,[4 100], sampling_rate);
    plot(t, data_band, 'color', p_color(1,:), 'LineWidth', 1)
    
    data_path       = [d_optimal(sample_idx).folder filesep d_optimal(sample_idx).name];
    load(data_path, 'data');
    data_seg        = data(channel,1:15*sampling_rate);
    data_band       = bandpass(data_seg,[4 100], sampling_rate);
    plot(t, data_band + .5e-3, 'color', p_color(2,:), 'LineWidth', 1)
    
    data_path       = [d_suboptimal(sample_idx).folder filesep d_suboptimal(sample_idx).name];
    load(data_path, 'data');
    data_seg        = data(channel,1:15*sampling_rate);
    data_band       = bandpass(data_seg,[4 100], sampling_rate);
    plot(t, data_band + 1e-3, 'color', p_color(3,:), 'LineWidth', 1)
    
    plot([5 10], 14e-4*[1 1], 'k', 'LineWidth', 2)
    plot([1 2], [-4e-4 -4e-4], 'LineWidth', 2, 'color', 'k')
    plot([1 1], [-4e-4 -3e-4], 'LineWidth', 2, 'color', 'k')
    
    yticklabels([])
    xlabel('Time (Seconds)')
    set(gca, 'FontSize', 16)
    
end

%%
sublabel_size   = 24;
y_coords        = [.88 .3 ];
x_coords        = [.06 .53];

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','c','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','d','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
