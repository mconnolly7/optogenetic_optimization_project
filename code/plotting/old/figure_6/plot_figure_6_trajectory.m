%% Organize data
clear; clc;
paper_dir               = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir          = [paper_dir 'data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/'];
optimization_data_dir   = [experiment_dir 'optimization_data/'];
raw_data_dir            = [optimization_data_dir 'raw_data/'];
processed_data_dir      = [optimization_data_dir 'processed_data/'];

data_dir            = 'optogenetic_optimization_project/data/in_vivo_data/ms_opto_gamma_maximization_nu_0.4/';
subject_dirs        = {'EPI032-GP' 'EPI033-GP'};


for c1 = 1:size(subject_dirs,2)
    subject_path    = [processed_data_dir subject_dirs{c1}];
    d               = dir(subject_path);
    
    for c2 = 1:size(d,1)        
        experiment_dir = d(c2).name;
        
        if ~strcmp(experiment_dir(1),'.') && d(c2).isdir
           experiment_path = [subject_path filesep experiment_dir];
          
           load([experiment_path filesep 'gp_model.mat'])
           
           [x_max_est, y_max_est]       = get_optimization_trajectory(gp_temp);
           
           plot_data(c1,c2).x_sample    = gp_temp.x_data;
           plot_data(c1,c2).y_sample    = gp_temp.y_data;
           
           plot_data(c1,c2).x_max_est   = x_max_est;
           plot_data(c1,c2).y_max_est   = y_max_est;
           
        end
    end
end

%%
results_dir   = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.4/';
figure_3_data = plot_data';
save([results_dir 'figure_3_data.mat'], 'figure_3_data');


%% Plot figure 3
close all; clc;

figure( 'Position',  [100, 100, 1000, 900])
p = [215,25,28
    43,131,186
    171,217,233]/255;

font_size = 12;
color_p = [215,25,28
253,174,97
171,221,164
43,131,186]/255;

results_dir        = 'optogenetic_optimization_project/results/in_vivo_results/ms_opto_gamma_maximization_nu_0.4/';

load([results_dir 'figure_3_data.mat']);
t_estimate = 11:30;

figure_3_data = [figure_3_data(:,2) figure_3_data(:,1)];
for c1 = 1:size(figure_3_data,2)
    offset = c1-1;
    for c2 = 1:size(figure_3_data,1)
        plot_data = figure_3_data(c2,c1);
        
        if isempty(plot_data.x_max_est)
            continue;
        end
        
        subplot(5,2,1+offset)
        plot(t_estimate, plot_data.x_max_est(t_estimate,1), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,1), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        subplot(5,2,3+offset)
        plot(t_estimate,plot_data.x_max_est(t_estimate,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,2), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        subplot(5,2,5+offset)
        plot(t_estimate, plot_data.x_max_est(t_estimate,3), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(plot_data.x_sample(:,3), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')
        
        subplot(5,2,7+offset)
        plot(t_estimate, log(plot_data.y_max_est(t_estimate,1)), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', '-');
        hold on
        plot(log(plot_data.y_sample(:,1)), 'LineWidth', 2, 'Color', p(c1,:), 'LineStyle', ':')

    end
end

for c1 = 1:8
    subplot(5,2,c1)
    patch([1 11 11 1], [0 0 60 60], .5*ones(1,3), 'FaceAlpha', .3)
end

subplot(5,2,1)
ylabel('Amplitude (V)')
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([4 5.2])
xticklabels({})
title('R4')
legend({'Estimated Optimal Parameter', 'Sampled Parameter'}, 'location', 'northwest')

subplot(5,2,2)
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([4 5.2])
xticklabels({})
yticklabels({})
title('R5')

subplot(5,2,3)
ylabel('Pulse Frequency (Hz)')
set(gca, 'Fontsize', font_size)
xticklabels({})
xlim([1 30])
ylim([0 50])

%%%%%
subplot(5,2,4)
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 50])
xticklabels({})
yticklabels({})

%%%%%
subplot(5,2,5)
ylabel('Pulse Width (ms)')
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 10])
xticklabels({})

%%%%%
subplot(5,2,6)
set(gca, 'Fontsize', font_size)
xlim([1 30])
ylim([0 10])
xticklabels({})
yticklabels({})

%%%%%
subplot(5,2,7)
xlim([1 30])
% ylim([0 1e-6])
xlabel('Samples')
ylabel('Gamma Power (V^2/Hz)')
set(gca, 'Fontsize', font_size)

%%%%%
subplot(5,2,8)
set(gca, 'Fontsize', font_size)
xlim([1 30])
% ylim([0 7e-7])
yticklabels({})
xticklabels({})

%%%%%
subplot(5,2,10)
plot(t_estimate, figure_3_data(4,2).y_max_est(t_estimate,1), 'LineWidth', 2, 'Color', p(2,:), 'LineStyle', '-');
hold on
plot(figure_3_data(4,2).y_sample(:,1), 'LineWidth', 2, 'Color', p(2,:), 'LineStyle', ':')
plot(t_estimate, figure_3_data(5,2).y_max_est(t_estimate,1), 'LineWidth', 2, 'Color', p(2,:), 'LineStyle', '-');
plot(figure_3_data(5,2).y_sample(:,1), 'LineWidth', 2, 'Color', p(2,:), 'LineStyle', ':')
patch([1 11 11 1], [0 0 60 60], .5*ones(1,3), 'FaceAlpha', .3)

ylim([0 6e-8])
xlim([1 30])
xticklabels({})
set(gca, 'Fontsize', font_size)

%%%%%
subplot(5,2,9)
plot_figure_3_spectrogram
title('')
xlabel('Seconds')
ylabel('Frequency (Hz)')
set(gca, 'Fontsize', font_size)

sublabel_size   = 24;
y_coords        = [.87 .68 .52 .34 .16];
x_coords        = [.06 .53];

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','a','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','b','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(3) .1 .1],'String','c','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(4) .1 .1],'String','d','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(5) .1 .1],'String','j','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','e','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','f','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(3) .1 .1],'String','g','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(4) .1 .1],'String','h','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(5) .1 .1],'String','i','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

