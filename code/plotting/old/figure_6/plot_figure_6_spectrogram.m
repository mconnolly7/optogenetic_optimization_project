params.Fs       = 2000;
params.tapers   = [3 5];
params.fpass    = [4 55];

paper_dir       = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
raw_dir         = [paper_dir 'results_data/Gamma power opto/Raw LFP/'];
sham_path       = [raw_dir 'EPI032_112018_tr1/Sham/Resampled_Compet_3param_19.mat'];

load(sham_path, 'data');
[S_sham, ~,~]   = mtspecgramc(data(1,:), [1 1], params);

exp_dir         = [paper_dir '/results_data/Gamma power opto/Raw LFP Opt/'];

experiment_dirs = dir([exp_dir 'E*']);
for c2 = 3;%1:size(experiment_dirs,1);
    
    data_dir = [exp_dir experiment_dirs(c2).name '/'];
    
    d   = dir([data_dir 'Resampl*']);

    S_all           = [];

    for c1 = 1:size(d,1)
        file_name = d(c1).name;
        file_path = [data_dir file_name];

        load(file_path, 'data');

        [S t f] = mtspecgramc(data(1,:), [1 1], params);
        S_all = [S_all; S_sham(10:29,:); S; S_sham(41:60,:);];
    %     S_all = [S_all; S ];
    end

    t_all = 1:1:size(S_all,1);
    
    plot_matrix(S_all, t_all, f);
end
colorbar off