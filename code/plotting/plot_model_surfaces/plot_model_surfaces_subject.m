clear

load('optogenetic_optimization_project/data/in_silico_data/processed_data/laxpati_2014/hSynChR2_PSD_COH_W20_Ch1-16_1-100Hz_1Hzbin.mat')

raw_data_dir        = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/data/in_silico_data/raw_data/R1/';
d_lfp{1}            = [raw_data_dir '10I_5Hz_2ms/'];
d_lfp{2}            = [raw_data_dir '30I_23Hz_5ms/'];
d_lfp{3}            = [raw_data_dir '50I_35Hz_10ms/'];

sampling_rate       = 2000;
exclude_idx         = [];
for c1 = 1:3
    d               = dir([d_lfp{c1} 'DRAG*']);
    
    for c2 = 1:size(d,1)
        f_name              = d(c2).name;
        data_path           = [d_lfp{c1} f_name];
        data                = Load_LFP({data_path});
        data_seg            = data{1}.data(2,1:119700);

        data_band(c1,c2,:)  = bandpass(data_seg,[1 100], sampling_rate);
    end
    

end

%%
close all

f                   = figure( 'Position',  [190, 100, 1600, 1000]);

lower               = [10 5];
upper               = [50 42];

x_grid              = [.05 .38 .71];
y_grid_1            = [.55 .05 .75];

width               = .25;
height              = [.4 .2];

ax(1)               = axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
ax(2)               = axes('Position', [x_grid(2)  y_grid_1(2) width(1) height(1)]);
ax(3)               = axes('Position', [x_grid(3)  y_grid_1(2) width(1) height(1)]);

ax(4)               = axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(5)               = axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(6)               = axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

%%%%
% A
%%%%

v       = viridis(31); % Get an arbitrary range of viridis colormap
v_idx   = [1 13 28];   % select 3 based on graph
v       = v(v_idx,:);  % re-use variable v 

set(f, 'currentaxes', ax(4));
hold on

offset              = [0 1 2];
rep_sample          = 1+[1 1 1];
for c1 = 3:-1:1
    plot_data_t     = (1:size(data_band(c1,1,:),3))/2000;
    plot_data_lfp   = squeeze(data_band(c1,rep_sample(c1),:)) + offset(c1)*1e-3;
    plot_idx(c1)   	= plot(plot_data_t, plot_data_lfp, 'color', v(c1,:), 'LineWidth', 1);
end

plot([20 40], 25e-4*[1 1], 'k:', 'LineWidth', 2)
plot([1 2], [-4e-4 -4e-4], 'LineWidth', 2, 'color', 'k')
plot([1 1], [-4e-4 -3e-4], 'LineWidth', 2, 'color', 'k')

yticklabels([])
yticks([])
xticks([])
set(gca, 'FontSize', 16)
set(gca,'visible','off')

l = legend(flip(plot_idx), {'50 mw/mm^2, 35 Hz, 10 ms' '30 mW/mm^2, 23 Hz, 5 ms', '10 mW/mm^2, 7 Hz, 2 ms'}, 'position', [0.1929 0.9084 0.1337 0.0710], 'Box', 'off');

set(f, 'currentaxes', ax(1));
ylabel('Frequency (Hz)')
xlabel('Amplitude (mW/mm^2)')
zlabel('Gamma Power (mV^2/Hz)')

%%%%
% B - C
%%%%

representative_freq = [5 23 42];

params.pass     = [1 100];
params.tapers   = [3 5];
params.Fs       = 2000;

for c1 = 3:-1:1
    
    %%%%
    % B
    %%%%
    set(f, 'currentaxes', ax(5));
    hold on

    for c2 = 1:size(data_band,2)
        lfp_data        = data_band(c1,c2,(2000*20):(2000*40));
        [PSD_data, F]   = mtspectrumc(lfp_data, params);
        PSD_log(c2,:)  = log10(PSD_data*1e6);
    end
    
    PSD_mean    = mean(PSD_log);
    PSD_std     = std(PSD_log);
    
    FF          = [F flip(F)];
    yy          = [PSD_mean - PSD_std flip(PSD_mean + PSD_std)];

    plot(F,PSD_mean, 'color', v(c1,:),'LineWidth', 2)
    box off
    
    plot([33 50; 33 50], [-6 -6; 0 0], 'k:')

    set(gca,'FontSize', 18)
    xlim([1  100])
    ylim([-5.7 -.5])
    xlabel('Frequency (Hz)')
    ylabel('log_{10}[Power (mV^2/Hz)]')
   
    
    %%%%
    % C
    %%%%
    set(f, 'currentaxes', ax(6));
    hold on

    patch(FF, yy, v(c1,:), 'FaceAlpha', .25, 'edgecolor', 'k', 'LineWidth', 0.5)
        
    plot(F,PSD_mean, 'color', v(c1,:),'LineWidth', 2)
    box off
    xlim([33  50])
    ylim([-5.7 -.5])
    set(gca,'FontSize', 18)
    xlabel('Frequency (Hz)')

end


%%%%
% Row 3
%%%%
unique_subjects = unique(stimtable(:,4));
for c1 = 1:3
    set(f, 'currentaxes', ax(c1));

    subject_idx = stimtable(:,4) == unique_subjects(c1);
    
    X_data      = stimtable(subject_idx,1:2);
    PSD_subject = squeeze(PSDall(subject_idx,2,1,33:50));
    
    Y_data      = nansum(nansum(PSD_subject,2),3) ;
    Y_data      = Y_data(~isnan(Y_data))*1e6;
    X_data      = X_data(~isnan(Y_data),:);
    
    objective_model    = gp_object();
    objective_model.initialize_data(X_data, Y_data)
    
    
    objective_model.plot_mean
    objective_model.plot_data
    colormap(viridis)
    title(sprintf('Subject R%d', c1))
    set(gca,'FontSize', 18)
    xlim([10 50])
    ylim([5 42])
end

set(f, 'currentaxes', ax(1));
xlabel('Amplitude (mW/mm^2)');
ylabel('Frequency (Hz)');
zlabel('Gamma Power (mV/Hz^2)');


sublabel_size   = 28;
y_coords        = [.88 .4 ];
x_coords        = [.029 .33 .66];

annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(3) y_coords(1) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(1) y_coords(2) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);


print('subject_models.eps', '-depsc')