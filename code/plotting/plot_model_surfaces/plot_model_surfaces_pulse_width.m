close all
f = figure( 'Position',  [190, 100, 1600, 500])

lower = [10 5];
upper = [50 42];

x_grid          = [.05 .38 .71];
y_grid_1        = [.10 ];

width           = .25;
height          = .8;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(2)  y_grid_1(1) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(3)  y_grid_1(1) width(1) height(1)]);

load('optogenetic_optimization_project/data/in_silico_data/processed_data/laxpati_2014/hSynChR2_PSD_COH_W20_Ch1-16_1-100Hz_1Hzbin.mat')
    
subject_idx = stimtable(:,4) == 1;

X_data      = stimtable(subject_idx,:);
PSD_subject = squeeze(PSDall(subject_idx,2,1,33:50));

Y_data      = nansum(nansum(PSD_subject,2),3) ;
Y_data      = Y_data(~isnan(Y_data))*1e6;
X_data      = X_data(~isnan(Y_data),:);
    
    
%
unique_pw = unique(X_data(:,3));
for c1 = 1:3
    set(f, 'currentaxes', ax(c1));
    
    pw_idx = X_data(:,3) == unique_pw(c1);

    X = X_data(pw_idx,[1 2]);
    Y = Y_data(pw_idx);
    
    objective_model = gp_object();
    objective_model.initialize_data(X, Y, lower, upper)
%     objective_model.minimize(10)
    
    objective_model.plot_mean
    objective_model.plot_data
    
    title(sprintf('Pulse Width %dms', unique_pw(c1)))
    set(gca,'FontSize', 18)
    xlim([10 50])
    ylim([5 42])
end

for c1 = 1:3
    set(f, 'currentaxes', ax(c1));
    zlim([0 1.1e-3])
end

set(f, 'currentaxes', ax(1));
ylabel('Frequency (Hz)')
xlabel('Amplitude (mW/mm^2)')
zlabel('Gamma Power (mV^2/Hz)')
    colormap(viridis)

print('pulse_width_figures', '-dpng', '-r500')
