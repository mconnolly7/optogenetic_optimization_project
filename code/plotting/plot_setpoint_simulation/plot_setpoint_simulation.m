% function plot_figure_5_setpoint_simulation

close all
load('optogenetic_optimization_project/data/in_silico_data/setpoint_data/R1_data_3D.mat');
d1              = [253,187,132
                239,101,72
                153,0,0]/255;
g1              = 0*ones(1,3);
f = figure( 'Position',  [190, 100, 1200, 1100]);

x_grid          = [.07 .56];
y_grid_1        = [.11 .32 .53 .74];
y_grid_2        = [.11 .55];
width           = .4;
height          = [.2 .39] ;

font_size       = 14;               
final_est_idx   = sample_idx_all == 40;

ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
ax(2)        	= axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
ax(3)        	= axes('Position', [x_grid(1)  y_grid_1(3) width(1) height(1)]);
ax(4)        	= axes('Position', [x_grid(1)  y_grid_1(4) width(1) height(1)]);
ax(5)        	= axes('Position', [x_grid(2)  y_grid_2(1) width(1) height(2)]);
ax(6)        	= axes('Position', [x_grid(2)  y_grid_2(2) width(1) height(2)]);

hold(ax(1),'on');hold(ax(2),'on');hold(ax(3),'on');
hold(ax(4),'on');hold(ax(5),'on');hold(ax(6),'on');

set(f, 'currentaxes', ax(6));

for c1 = 1:n_setpoints
    setpoint_idx    = setpoint_all == setpoints(c1);
    plot_idx        = setpoint_idx & final_est_idx;
    
    scatter3(X_optimal_all(plot_idx,1), X_optimal_all(plot_idx,2), 3*ones(sum(plot_idx),1),...
        200, d1(c1,:), 'filled', 'MarkerFaceAlpha', .7, 'MarkerEdgeColor', 'k', 'LineWidth', 1)
    plot3(x_iso_1{1,c1}, x_iso_2{1,c1}, ones(size(x_iso_2{1,c1})),'color', d1(c1,:), 'LineWidth', 5, 'LineStyle', '-')
end

ylim([5  42])
xlabel('Amplitude (mW/mm^2)')
ylabel('Frequency (Hz)')

set(ax(6), 'FontSize', font_size)
colormap('hot')

offset      = 5;
rep_trial   = 5;
line_style  = {'-', ':', '--'};
line_width  = 3;

for c1 = 1:n_setpoints
    setpoint_idx        = setpoint_all == setpoints(c1);
    
    for c2 = 1:12 
        trial_idx       = trial_idx_all == c2;
        
        opt_trial_1     = X_optimal_all(trial_idx & setpoint_idx, 1);
        opt_trial_2     = X_optimal_all(trial_idx & setpoint_idx, 2);
        opt_trial_3     = X_optimal_all(trial_idx & setpoint_idx, 3);
        
        smp_trial_1     = X_samples_all(trial_idx & setpoint_idx, 1);
        smp_trial_2     = X_samples_all(trial_idx & setpoint_idx, 2);
        smp_trial_3     = X_samples_all(trial_idx & setpoint_idx, 3);
        
        gt_trial        = y_gt_all(trial_idx & setpoint_idx);
        smp_trial       = Y_samples_all(trial_idx & setpoint_idx);
        
        sample_idx      = n_burn_in:n_samples ;
        
        if any(c2 == rep_trial)
            plot_color  = d1(c1,:);
     
            plot(ax(4),sample_idx+ (c1-1)*(n_samples+5), smp_trial_1(sample_idx), ...
                'color', g1, 'LineWidth', 1, 'linestyle', line_style{1})            
            plot(ax(4),sample_idx+ (c1-1)*(n_samples+5), opt_trial_1(sample_idx), ...
                'color', plot_color, 'LineWidth', line_width, 'linestyle', line_style{1})     

            plot(ax(3),sample_idx+ (c1-1)*(n_samples+5), smp_trial_2(sample_idx), ...
                'color', g1, 'LineWidth', 1, 'linestyle', line_style{1})
            plot(ax(3),sample_idx+ (c1-1)*(n_samples+5), opt_trial_2(sample_idx), ...
                'color', plot_color, 'LineWidth', line_width, 'linestyle', line_style{1})  
            
            plot(ax(2),sample_idx+ (c1-1)*(n_samples+5), smp_trial_3(sample_idx), ...
                'color', g1, 'LineWidth', 1, 'linestyle', line_style{1})
            plot(ax(2),sample_idx+ (c1-1)*(n_samples+5), opt_trial_3(sample_idx), ...
                'color', plot_color, 'LineWidth', line_width, 'linestyle', line_style{1})  

            plot(ax(1),sample_idx+ (c1-1)*(n_samples+5), smp_trial(sample_idx), ...
                'color', g1, 'LineWidth', 1, 'linestyle', line_style{1})
            plot_handle(c1) = plot(ax(1),sample_idx+ (c1-1)*(n_samples+5), gt_trial(sample_idx), ...
                'color', plot_color, 'LineWidth', line_width, 'linestyle', line_style{1});
        end
        
        y_gt_end(c2,c1) = gt_trial(end);
        
    end
end

%%%%%%%%%%%%
% Labeling %
%%%%%%%%%%%%
set(f, 'currentaxes', ax(4));
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');

xlim([1 129])
ylim([10 50])
xticks([1 10 40 46 55 85 91 100 130])
xticklabels({})
ylabel('Amplitude (mW/mm^2)')
set(gca,'FontSize', font_size)

set(f, 'currentaxes', ax(3));
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
xlim([1 129])
ylim([5 42])
xticks([1 10 40 46 55 85 91 100 130])
xticklabels({})
ylabel('Frequency (Hz)')
set(gca,'FontSize', font_size)

set(f, 'currentaxes', ax(2));
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
xlim([1 129])
ylim([2 10])
xticks([1 10 40 46 55 85 91 100 130])
xticklabels({})
ylabel('Pulse Width (ms)')
set(gca,'FontSize', font_size)

set(f, 'currentaxes', ax(1));
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
xlim([1 129])
ylim([0 1])
xticks([1 10 40 46 55 85 91 100 130])
xticklabels({'1', '10', '40'})
xlabel('Samples')
ylabel('Gamma (scaled)')
set(gca,'FontSize', font_size)


%%%%%%%%%
% Optimal performance
%%%%%%%%%

set(f, 'currentaxes', ax(5));
hold on
marker_size = 200;
mfa         = .5;
x_div       = 4;
line_width  = 3;

for c1 = 1:3
    y_data      = y_gt_end(:,c1);
    m1(c1)      = mean(y_data);
    s1(c1)      = std(y_data);
    
    n_trials    = size(y_data, 1);
    x_idx       = c1*ones(n_trials,1) - rand(n_trials,1)/x_div + 1/x_div/2;
    scatter(x_idx, y_data, marker_size, d1(c1,:), 'filled', 'MarkerFaceAlpha', mfa, 'MarkerEdgeColor', 'k')
    plot([c1 c1], [m1(c1)-s1(c1) m1(c1)+s1(c1)], 'k', 'LineWidth', line_width)
end

plot([1 2 3], m1, 'k', 'LineWidth', line_width)
title('')
xticks([1 2 3]);
xticklabels({'0.15', '0.45', '0.8'})
xlabel('Setpoint')
ylabel('Gamma (Scaled)')
set(gca,'FontSize', font_size)

y_coords        = [.89  .45 ];
x_coords        = [.03 .5];
sublabel_size   = 30;
annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);

set(f, 'currentaxes', ax(1));
l = legend(plot_handle, {'Y_{SP} = 0.15', 'Y_{SP} = 0.45','Y_{SP} = 0.8',},...
    'NumColumns', 3, 'Box', 'off', 'FontSize', font_size, 'Position', [0.1242 0.9514 0.2683 0.0255])

print('setpoint_simulation', '-depsc')


