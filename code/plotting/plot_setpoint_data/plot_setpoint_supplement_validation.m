function [outputArg1,outputArg2] = plot_setpoint_supplement_validation(inputArg1,inputArg2)
rng(0)
close all
clear

paper_dir           = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
data_dir            = [paper_dir 'data/in_vivo_data/ms_opto_gamma_setpoint/'];
optimization_dir    = [data_dir 'optimization_phase/'];    
data_files          = {'EPI033_2019_09_30.mat', 'EPI033_2019_10_01', 'EPI047_2019_11_11', 'EPI047_2019_11_12'};

intensity_map       = [49.6474, -189.0011];

exp_day             = 2;

d1                  = [253,187,132
                    239,101,72
                    153,0,0]/255;

p1                  = [158,188,218
                    140,107,177
                    110,1,107]/255;

mfa                 = 1;               
font_size           = 13;

f                   = figure( 'Position',  [190, 100, 800, 1000]);

%%%%

x_grid          = [.07 .56];
y_grid_1        = [.07 .28 .45 .62 .79];
y_grid_2        = [.07 .28 .64];

width           = .4;
height          = [.16 .31] ;

% ax(1)        	= axes('Position', [x_grid(1)  y_grid_1(1) width(1) height(1)]);
% ax(2)        	= axes('Position', [x_grid(1)  y_grid_1(2) width(1) height(1)]);
% ax(3)        	= axes('Position', [x_grid(1)  y_grid_1(3) width(1) height(1)]);
% ax(4)        	= axes('Position', [x_grid(1)  y_grid_1(4) width(1) height(1)]);
% ax(5)        	= axes('Position', [x_grid(1)  y_grid_1(5) width(1) height(1)]);
% ax(6)        	= axes('Position', [x_grid(2)  y_grid_2(1) width(1) height(1)]);
% ax(7)        	= axes('Position', [x_grid(2)  y_grid_2(2) width(1) height(2)]);
% ax(8)        	= axes('Position', [x_grid(2)  y_grid_2(3) width(1) height(2)]);
% 
% hold(ax(1),'on');hold(ax(2),'on');hold(ax(3),'on');hold(ax(4),'on');
% hold(ax(5),'on');hold(ax(6),'on');hold(ax(7),'on');hold(ax(8),'on');
% 
% %%%%
% 
% for c1 = 1:size(data_files,2)
%     file_path       = [optimization_dir data_files{c1}];
%     data_tables(c1) = load(file_path);
% end
% 
% baseline(1) = data_tables(1).data_table.baseline(1);
% baseline(2) = data_tables(2).data_table.baseline(1);
% baseline(3) = data_tables(3).data_table.baseline_vec(1);
% 
% %%%%%%%%
% %
% %%%%%%%%
% set(f, 'currentaxes', ax(5));
% plot(1:40, data_tables(exp_day).data_table.intensity_sample(1:40), 'k')
% plot(46:85, data_tables(exp_day).data_table.intensity_sample(41:80), 'k')
% plot(91:130, data_tables(exp_day).data_table.intensity_sample(81:120), 'k')
% 
% plot(1:40, data_tables(exp_day).data_table.intensity_est(1:40), 'LineWidth', 3, 'color', d1(1,:))
% plot(46:85, data_tables(exp_day).data_table.intensity_est(41:80), 'LineWidth', 3, 'color', d1(2,:))
% plot(91:130, data_tables(exp_day).data_table.intensity_est(81:120), 'LineWidth', 3, 'color', d1(3,:))
% 
% scatter(40, data_tables(exp_day).data_table.intensity_est(40), 200, d1(1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
% scatter(85, data_tables(exp_day).data_table.intensity_est(80), 200, d1(2,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
% scatter(130, data_tables(exp_day).data_table.intensity_est(120), 200, d1(3,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
%     
% patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% xticks([1 10 40])
% 
% xticklabels([]);
% xlim([1 130])
% ylim([9 60]);
% ylabel('Amplitude (mW/mm^2)')
% set(gca, 'FontSize', font_size)
% 
% %%%%%%%%%
% set(f, 'currentaxes', ax(4));
% plot(1:40, data_tables(exp_day).data_table.frequency_sample(1:40), 'k')
% plot(46:85, data_tables(exp_day).data_table.frequency_sample(41:80), 'k')
% plot(91:130, data_tables(exp_day).data_table.frequency_sample(81:120), 'k')
% 
% plot(1:40, data_tables(exp_day).data_table.frequency_est(1:40), 'LineWidth', 3, 'color', d1(1,:))
% plot(46:85, data_tables(exp_day).data_table.frequency_est(41:80), 'LineWidth', 3, 'color', d1(2,:))
% plot(91:130, data_tables(exp_day).data_table.frequency_est(81:120), 'LineWidth', 3, 'color', d1(3,:))
% 
% scatter(40, data_tables(exp_day).data_table.frequency_est(40), 200, d1(1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
% scatter(85, data_tables(exp_day).data_table.frequency_est(80), 200, d1(2,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
% scatter(130, data_tables(exp_day).data_table.frequency_est(120), 200, d1(3,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255)
%     
% patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% 
% xticks([1 10 40])
% xticklabels([]);
% xlim([1 130])
% ylim([1 45])
% ylabel('Frequency (Hz)')
% set(gca, 'FontSize', font_size)
% 
% %%%%%%
% set(f, 'currentaxes', ax(3));
% plot(1:40, data_tables(exp_day).data_table.pulse_width_sample(1:40), 'k')
% plot(46:85, data_tables(exp_day).data_table.pulse_width_sample(41:80), 'k')
% plot(91:130, data_tables(exp_day).data_table.pulse_width_sample(81:120), 'k')
% 
% plot(1:40, data_tables(exp_day).data_table.pulse_width_est(1:40), 'LineWidth', 3, 'color', d1(1,:))
% plot(46:85, data_tables(exp_day).data_table.pulse_width_est(41:80), 'LineWidth', 3, 'color', d1(2,:))
% plot(91:130, data_tables(exp_day).data_table.pulse_width_est(81:120), 'LineWidth', 3, 'color', d1(3,:))
% 
% patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% 
% xticks([1 10 40])
% xticklabels([]);
% xlim([1 130])
% ylim([2 8])
% ylabel('Pulse Width (ms)')
% set(gca, 'FontSize', font_size)
% 
% %%%%%
% set(f, 'currentaxes', ax(2));
% plot(1:40, data_tables(exp_day).data_table.gamma_sample(1:40)/baseline(exp_day), 'k')
% plot(46:85, data_tables(exp_day).data_table.gamma_sample(41:80)/baseline(exp_day), 'k')
% plot(91:130, data_tables(exp_day).data_table.gamma_sample(81:120)/baseline(exp_day), 'k')
% 
% plot_handle(1) = plot(1:40, data_tables(exp_day).data_table.gamma_est(1:40)/baseline(exp_day), 'LineWidth', 3, 'color', d1(1,:));
% plot_handle(2) = plot(46:85, data_tables(exp_day).data_table.gamma_est(41:80)/baseline(exp_day), 'LineWidth', 3, 'color', d1(2,:));
% plot_handle(3) = plot(91:130, data_tables(exp_day).data_table.gamma_est(81:120)/baseline(exp_day), 'LineWidth', 3, 'color', d1(3,:));
% 
% patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
% 
% xlim([1 130])
% ylim([0 10])
% ylabel('Gamma / Baseline')
% xlabel('Samples')
% 
% xticks([1 10 40])
% xticklabels({'1', '10', '40'})
% set(gca, 'FontSize', font_size)
% 
% %%%%%%%%
% %
% %%%%%%%%
% intensity_est_1         = data_tables(1).data_table.intensity_est([40 80 120]);
% intensity_est_2         = data_tables(2).data_table.intensity_est([40 80 120]);
% intensity_est_3         = data_tables(3).data_table.intensity_est([40 80 120]-4);
% intensity_est_4         = data_tables(4).data_table.intensity_est([40 80 120]);
% 
% frequency_estimate_1    = data_tables(1).data_table.frequency_est([40 80 120]); 
% frequency_estimate_2    = data_tables(2).data_table.frequency_est([40 80 120]); 
% frequency_estimate_3    = data_tables(3).data_table.frequency_est([40 80 120]-4); 
% frequency_estimate_4    = data_tables(4).data_table.frequency_est([40 80 120]); 
% 
% set(f, 'currentaxes', ax(8));
%  
% markers     = {'^','o','s'};
% 
% 
% 
% plot(intensity_est_1 , frequency_estimate_1,'linewidth', 1, 'color', 'k', 'LineStyle', ':',...
%     'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot(intensity_est_2 , frequency_estimate_2,'linewidth', 1, 'color', 'k', 'LineStyle', '-.',...
%     'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot(intensity_est_3 , frequency_estimate_3,'linewidth', 1, 'color', 'k', 'LineStyle', '-',...
%     'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot(intensity_est_4 , frequency_estimate_4,'linewidth', .5, 'color', 'k', 'LineStyle', '--',...
%     'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% 
% plot_handle_2(1) = plot(0 , 0,'linewidth', 1, 'color', 'k', 'LineStyle', ':',...
%     'Marker', 'd', 'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot_handle_2(2) = plot(0 , 0,'linewidth', 1, 'color', 'k', 'LineStyle', '-.',...
%     'Marker', 's', 'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot_handle_2(3) = plot(0 , 0,'linewidth', 1, 'color', 'k', 'LineStyle', '-',...
%     'Marker', '^', 'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% plot_handle_2(4) = plot(0 , 0,'linewidth', .5, 'color', 'k', 'LineStyle', '--',...
%     'Marker', 'o', 'MarkerEdgeColor', 'k', 'MarkerSize', 13);
% 
% for c1 = 1:3
%     
%     scatter(intensity_est_1(c1), frequency_estimate_1(c1), 200, d1(c1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 1, 'Marker', 'd', 'MarkerEdgeColor', 'k'); 
%     scatter(intensity_est_2(c1), frequency_estimate_2(c1), 200, d1(c1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', 's', 'MarkerEdgeColor', [0,109,44]/255);
%     
%     scatter(intensity_est_3(c1), frequency_estimate_3(c1), 200, d1(c1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 1, 'Marker', '^', 'MarkerEdgeColor', 'k');
%     scatter(intensity_est_4(c1), frequency_estimate_4(c1), 200, d1(c1,:), 'filled', ...
%         'MarkerFaceAlpha', mfa, 'LineWidth', 1, 'Marker', 'o', 'MarkerEdgeColor', 'k');
% end
% 
% legend(plot_handle_2, {'R5 Trial: 1','R5 Trial: 2','R6 Trial: 1','R6 Trial: 2'}, 'box' ,'off')
% xlabel('Amplitude (mW/mm^2)')
% ylabel('Frequency (Hz)')
% xlim([9 60])
% set(gca, 'FontSize', font_size)

%%%%%%
clear data_tables
test_dir = [data_dir 'test_phase/'];
for c1 = 1:size(data_files,2)
    file_path       = [test_dir data_files{c1}];
    data_tables(c1) = load(file_path);
end

% set(f, 'currentaxes', ax(7));

marker_size = 150;
mfa         = .7;
x_div       = 4;
x_off       = .1;
D           = [150 300 2000];
line_width  = 3;
baseline(1) = data_tables(1).test_table.Var2(1);
baseline(2) = data_tables(2).test_table.Var2(1);

baseline(3) = data_tables(3).test_table.Var2(1);
baseline(4) = data_tables(4).test_table.Var2(1);
%%%%%
hold on
for c1 = 1:3
    x_idx   = c1*ones(10,1) - rand(10,1)/x_div - x_off;
    d_idx   = data_tables(3).test_table.gamma_mult == D(c1);
    y_data  = data_tables(3).test_table.gamma_sample(d_idx)/baseline(3);
    
    m1(c1)    = mean(y_data);
    s1(c1)    = std(y_data);
    
    scatter(x_idx, y_data, marker_size, d1(c1,:), 'filled', 'MarkerFaceAlpha', mfa, 'marker', '^',...
        'MarkerEdgeColor', 'k')    
    plot([c1 c1] - x_off - 1/x_div/2, [m1(c1)-s1(c1) m1(c1)+s1(c1)], 'k', 'LineWidth', line_width)
    
    x_idx   = c1*ones(10,1) + rand(10,1)/x_div + x_off;
    d_idx   = data_tables(4).test_table.gamma_mult == D(c1);
    y_data  = data_tables(4).test_table.gamma_sample(d_idx)/baseline(4);
    
    m2(c1)    = mean(y_data);
    s2(c1)    = std(y_data);
        
    scatter(x_idx, y_data, marker_size, d1(c1,:), 'filled', 'MarkerFaceAlpha', mfa, 'marker', 'o', ...
        'MarkerEdgeColor', 'k')
    plot([c1 c1] + x_off + 1/x_div/2, [m2(c1)-s2(c1) m2(c1)+s2(c1)], 'k', 'LineWidth', line_width)

    if c1 < 3
        plot(c1, D(c1)/100, 'kx', 'MarkerSize', 10, 'LineWidth', line_width)
    end
end
plot([1 2 3] - x_off - 1/x_div/2, m1, 'k:', 'LineWidth', line_width)
plot([1 2 3] + x_off + 1/x_div/2, m2, 'k-.', 'LineWidth', line_width)

set(gca,'FontSize', font_size)

xticks([1 2 3]);
xticklabels({'1.5', '3.0', '20.0'})
xlabel('Baseline Multiple (Y_{BL})')

ylabel('Gamma / Baseline')
%%%%%%%%%

% for c1 = 1:3
%     
%     d_idx   = data_tables(exp_day).test_table.gamma_mult == D(c1);
%     S       = data_tables(exp_day).test_table.psd(d_idx,:)*1e6;
%     F       = data_tables(exp_day).test_table.F(1,:);
%     
%     m       = mean(S);
%     s       = std(S)/sqrt(10)*3.169;
%     
%     set(f, 'currentaxes', ax(1));
%     plot(F,log10(m), 'color', d1(c1,:))
%     
%     
%     set(f, 'currentaxes', ax(6));
%     
%     FF = [F flip(F)];
%     YY = [log10(m-s) flip(log10(m+s))];
%     
%     patch(FF, YY, d1(c1,:), 'facealpha', .5)
%     plot(F,log10(m), 'color', d1(c1,:), 'LineWidth', 1)
% 
% end
% set(f, 'currentaxes', ax(1));
% xlim([3 100])
%     
% set(gca,'FontSize', font_size)
% xlabel('Frequency (Hz)');
% ylabel('log_{10}[Power (mV/Hz^2)]');
% 
% plot([33 33], [-7 -3.8], 'k:')
% plot([50 50], [-7 -3.8], 'k:')
% ylim([-7 -3.8])
% 
% set(f, 'currentaxes', ax(6));
% xlabel('Frequency (Hz)');
% yl = ylabel('log_{10}[Power (mV/Hz^2)]');
% %     yl.Position(1)   = .17;
% xlim([33 50])
% set(gca,'FontSize', font_size)
% ylim([-7 -3.8])
%     
% y_coords        = [.89  .53  .17];
% x_coords        = [.02 .5];
% sublabel_size   = 24;
% 
% annotation('textbox',[x_coords(1) y_coords(1) .1 .1],'String','A','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(1) y_coords(3) .1 .1],'String','D','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% 
% annotation('textbox',[x_coords(2) y_coords(1) .1 .1],'String','B','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(2) y_coords(2) .1 .1],'String','C','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% annotation('textbox',[x_coords(2) y_coords(3) .1 .1],'String','E','FitBoxToText','on', 'EdgeColor', 'none', 'FontSize', sublabel_size);
% 
% set(f, 'currentaxes', ax(1));
% legend(plot_handle, {'1.5*Y_{BL}', '3.0*Y_{BL}','20.0*Y_{BL}',},...
%     'NumColumns', 3, 'Box', 'off', 'FontSize', font_size)


print('supplement_validation', '-dpng', '-r500')

end

