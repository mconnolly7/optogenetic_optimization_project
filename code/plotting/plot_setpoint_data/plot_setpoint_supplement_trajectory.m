function plot_setpoint_supplement_trajectory
rng(0)
close all
clear

paper_dir           = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
data_dir            = [paper_dir 'data/in_vivo_data/ms_opto_gamma_setpoint/'];
optimization_dir    = [data_dir 'optimization_phase/'];    
data_files          = {'EPI033_2019_09_30.mat', 'EPI033_2019_10_01', 'EPI047_2019_11_11', 'EPI047_2019_11_12'};

intensity_map       = [49.6474, -189.0011];

exp_day             = 2;

d1                  = [253,187,132
                    239,101,72
                    153,0,0]/255;

p1                  = [158,188,218
                    140,107,177
                    110,1,107]/255;

mfa                 = 1;               
font_size           = 13;

close all
f                   = figure( 'Position',  [190, 100, 1200, 1000]);

%%%%

x_grid          = [.05 .37 .69];
y_grid_1        = flip([.05  0.2750    0.5000    0.7250]);

width           = .27;
height          = [.215 .31] ;

for c1 = 1:3
    ax(c1,1)        	= axes('Position', [x_grid(c1)  y_grid_1(1) width(1) height(1)]);
    ax(c1,2)        	= axes('Position', [x_grid(c1)  y_grid_1(2) width(1) height(1)]);
    ax(c1,3)        	= axes('Position', [x_grid(c1)  y_grid_1(3) width(1) height(1)]);
    ax(c1,4)        	= axes('Position', [x_grid(c1)  y_grid_1(4) width(1) height(1)]);
end
%

for c1 = 1:numel(ax)
   c1
   hold(ax(c1), 'on')
end


%%%%

for c1 = 1:size(data_files,2)
    file_path       = [optimization_dir data_files{c1}];
    data_tables(c1) = load(file_path);
end

baseline(1) = data_tables(1).data_table.baseline(1);
baseline(2) = data_tables(2).data_table.baseline(1);
baseline(3) = data_tables(3).data_table.baseline_vec(1);
baseline(4) = data_tables(3).data_table.baseline_vec(1);

%%%%%%%%
%
%%%%%%%%
y_max = [10 5 15];
marker = {'d', '^', 'o'};
exp_days = [1 3 4];
for c1 = 1:3
    exp_day = exp_days(c1);
    set(f, 'currentaxes', ax(c1,1));
    range_1 = data_tables(exp_day).data_table.gamma_mult == 1.5;
    range_2 = data_tables(exp_day).data_table.gamma_mult == 3;
    range_3 = data_tables(exp_day).data_table.gamma_mult == 20;
    
    offset_1 = 1:sum(range_1);
    offset_2 = (1:sum(range_2)) + max(offset_1)+5;
    offset_3 = (1:sum(range_3)) + max(offset_2)+5;
    
    plot(offset_1, data_tables(exp_day).data_table.intensity_sample(range_1), 'k')
    plot(offset_2, data_tables(exp_day).data_table.intensity_sample(range_2), 'k')
    plot(offset_3, data_tables(exp_day).data_table.intensity_sample(range_3), 'k')

    plot(offset_1, data_tables(exp_day).data_table.intensity_est(range_1), 'LineWidth', 3, 'color', d1(1,:))
    plot(offset_2, data_tables(exp_day).data_table.intensity_est(range_2), 'LineWidth', 3, 'color', d1(2,:))
    plot(offset_3, data_tables(exp_day).data_table.intensity_est(range_3), 'LineWidth', 3, 'color', d1(3,:))

    scatter(offset_1(end), data_tables(exp_day).data_table.intensity_est(find(range_1,1,'last')), 200, d1(1,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)
    scatter(offset_2(end), data_tables(exp_day).data_table.intensity_est(find(range_2,1,'last')), 200, d1(2,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)
    scatter(offset_3(end), data_tables(exp_day).data_table.intensity_est(find(range_3,1,'last')), 200, d1(3,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)

    patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    xticks([1 10 40])

    xticklabels([]);
    xlim([1 130])
    ylim([9 60]);
    ylabel('Amplitude (mW/mm^2)')
    set(gca, 'FontSize', font_size)

    %%%%%%%%%
    set(f, 'currentaxes', ax(c1,2));
    plot(offset_1, data_tables(exp_day).data_table.frequency_sample(range_1), 'k')
    plot(offset_2, data_tables(exp_day).data_table.frequency_sample(range_2), 'k')
    plot(offset_3, data_tables(exp_day).data_table.frequency_sample(range_3), 'k')

    plot(offset_1, data_tables(exp_day).data_table.frequency_est(range_1), 'LineWidth', 3, 'color', d1(1,:))
    plot(offset_2, data_tables(exp_day).data_table.frequency_est(range_2), 'LineWidth', 3, 'color', d1(2,:))
    plot(offset_3, data_tables(exp_day).data_table.frequency_est(range_3), 'LineWidth', 3, 'color', d1(3,:))

    scatter(offset_1(end), data_tables(exp_day).data_table.frequency_est(find(range_1,1,'last')), 200, d1(1,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)
    scatter(offset_2(end), data_tables(exp_day).data_table.frequency_est(find(range_2,1,'last')), 200, d1(2,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)
    scatter(offset_3(end), data_tables(exp_day).data_table.frequency_est(find(range_3,1,'last')), 200, d1(3,:), 'filled', ...
            'MarkerFaceAlpha', mfa, 'LineWidth', 3, 'Marker', marker{c1}, 'MarkerEdgeColor', [0,109,44]/255)

    patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');

    xticks([1 10 40])
    xticklabels([]);
    xlim([1 130])
    ylim([1 45])
    ylabel('Frequency (Hz)')
    set(gca, 'FontSize', font_size)

    %%%%%%
    set(f, 'currentaxes', ax(c1,3));
    plot(offset_1, data_tables(exp_day).data_table.pulse_width_sample(range_1), 'k')
    plot(offset_2, data_tables(exp_day).data_table.pulse_width_sample(range_2), 'k')
    plot(offset_3, data_tables(exp_day).data_table.pulse_width_sample(range_3), 'k')

    plot(offset_1, data_tables(exp_day).data_table.pulse_width_est(range_1), 'LineWidth', 3, 'color', d1(1,:))
    plot(offset_2, data_tables(exp_day).data_table.pulse_width_est(range_2), 'LineWidth', 3, 'color', d1(2,:))
    plot(offset_3, data_tables(exp_day).data_table.pulse_width_est(range_3), 'LineWidth', 3, 'color', d1(3,:))

    patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');

    xticks([1 10 40])
    xticklabels([]);
    xlim([1 130])
    ylim([2 10])
    ylabel('Pulse Width (ms)')
    set(gca, 'FontSize', font_size)

    %%%%%
    set(f, 'currentaxes', ax(c1,4));
    plot(offset_1, data_tables(exp_day).data_table.gamma_sample(range_1)/baseline(exp_day), 'k')
    plot(offset_2, data_tables(exp_day).data_table.gamma_sample(range_2)/baseline(exp_day), 'k')
    plot(offset_3, data_tables(exp_day).data_table.gamma_sample(range_3)/baseline(exp_day), 'k')

    plot_handle(1) = plot(offset_1, data_tables(exp_day).data_table.gamma_est(range_1)/baseline(exp_day), 'LineWidth', 3, 'color', d1(1,:));
    plot_handle(2) = plot(offset_2, data_tables(exp_day).data_table.gamma_est(range_2)/baseline(exp_day), 'LineWidth', 3, 'color', d1(2,:));
    plot_handle(3) = plot(offset_3, data_tables(exp_day).data_table.gamma_est(range_3)/baseline(exp_day), 'LineWidth', 3, 'color', d1(3,:));

    patch([offset_1(1) offset_1(1)+10 offset_1(1)+10 offset_1(1)], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([offset_2(1) offset_2(1)+10 offset_2(1)+10 offset_2(1)], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
    patch([offset_3(1) offset_3(1)+10 offset_3(1)+10 offset_3(1)], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');

    xlim([1 130])
    ylim([0 y_max(c1)])
    ylabel('Gamma / Baseline')
    xlabel('Samples')

    xticks([1 10 40])
    xticklabels({'1', '10', '40'})
    set(gca, 'FontSize', font_size)
end

    set(f, 'currentaxes', ax(1,1));
title('R5 Trial 1')
set(f, 'currentaxes', ax(2,1));
title('R6 Trial 1')
set(f, 'currentaxes', ax(3,1));
title('R6 Trial 2')

l=legend(plot_handle, {'1.5*Y_{BL}', '3.0*Y_{BL}','20.0*Y_{BL}',},'NumColumns', 3, 'Box', 'off', ...
    'position', [0.4975 0.9421 0.3312 0.0260], 'FontSize', font_size);

print('supplement_setpoint_trajectory', '-dpng', '-r500')

end

