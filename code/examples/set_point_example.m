% %%
clc;
clear;
close all;
rng(1);

% Configure experiment 

data_dir        = 'optogenetic_optimization_project/data/in_silico_data/modeled_data/gamma_3D/';

data_path       = [data_dir 'R' num2str(1) '_gamma_3D.mat'];

load(data_path);
X_gt            = gp_model.x_data;
Y_gt            = gp_model.y_data;

objective_model = gp_object;
objective_model.initialize_data(X_gt, Y_gt);


%%
n_trials        = 12;
setpoints       = [.15 .45 .8];

n_samples       = 40;
n_burn_in       = 10;
n_setpoints     = size(setpoints,2);

X_samples_all   = [];
Y_samples_all   = [];
X_optimal_all   = [];
y_gt_all        = [];

sample_idx_all  = [];
trial_idx_all   = [];
setpoint_all    = [];

% % run optimization

for c1 = 1:n_setpoints


    [X_samples, Y_samples, X_optimal_est, y_ground_truth]       ...
        = bayes_opt_loop(objective_model, n_trials, n_samples, n_burn_in, [3 .4], setpoints(c1));
    t   = toc;
    
    sample_idx      = repmat(1:n_samples, 1, n_trials)';
    trial_idx       = reshape(repmat(1:n_trials, n_samples, 1), [], 1);
    m_samples       = n_trials*n_samples;
    setpoint_mat    = setpoints(c1) * ones(m_samples,1);
    
    sample_idx_all  = [sample_idx_all; sample_idx];
    trial_idx_all   = [trial_idx_all; trial_idx];
    setpoint_all    = [setpoint_all; setpoint_mat];
    
    X_sample_1      = reshape(squeeze(X_samples(:,1,:)),[],1);
    X_sample_2      = reshape(squeeze(X_samples(:,2,:)),[],1);
    X_sample_3      = reshape(squeeze(X_samples(:,3,:)),[],1);
    X_sample_table  = [X_sample_1 X_sample_2 X_sample_3];
    
    X_samples_all   = [X_samples_all; X_sample_table];
    Y_samples_all   = [Y_samples_all; reshape(Y_samples, m_samples, 1)];
    
    X_optimal_1      = reshape(squeeze(X_optimal_est(:,1,:)),[],1);
    X_optimal_2      = reshape(squeeze(X_optimal_est(:,2,:)),[],1);
    X_optimal_3      = reshape(squeeze(X_optimal_est(:,3,:)),[],1);
    X_optimal_table  = [X_optimal_1 X_optimal_2 X_optimal_3];
    
    X_optimal_all   = [X_optimal_all; X_optimal_table];
    y_gt_all        = [y_gt_all; reshape(y_ground_truth, m_samples, 1)];

end     

%%
close all
d1              = [253,187,132
                239,101,72
                153,0,0]/255;

font_size       = 13;               
final_est_idx   = sample_idx_all == 40;

hold on
objective_model.plot_mean

for c1 = 1:n_setpoints
    setpoint_idx    = setpoint_all == setpoints(c1);
    plot_idx        = setpoint_idx & final_est_idx;
    
    scatter3(X_optimal_all(plot_idx,1), X_optimal_all(plot_idx,2), 3*ones(sum(plot_idx),1),...
        200, d1(c1,:), 'filled', 'MarkerFaceAlpha', 1, 'MarkerEdgeColor', 'k', 'LineWidth', 2)
end

ylim([5  42])
xlabel('Amplitude (mW/mm^2)')
ylabel('Frequency (Hz)')

set(gca, 'FontSize', font_size)
%%
subplot(3,1,1); hold on
subplot(3,1,2); hold on
subplot(3,1,3); hold on

offset      = 5;
rep_trial   = [12 12 12];

for c1 = 1:n_setpoints
    setpoint_idx    = setpoint_all == setpoints(c1);
    
    for c2 = 1:n_trials
        trial_idx       = trial_idx_all == c2;
        
        opt_trial_1     = X_optimal_all(trial_idx & setpoint_idx, 1);
        opt_trial_2     = X_optimal_all(trial_idx & setpoint_idx, 2);
        gt_trial        = y_gt_all(trial_idx & setpoint_idx);
        
        sample_idx      = n_burn_in:n_samples ;
        
        if c2 == rep_trial(c1)
            subplot(3,1,1)
            plot(sample_idx+ (c1-1)*(n_samples+5), opt_trial_1(sample_idx), 'color', d1(c1,:), 'LineWidth', 3)
            
            subplot(3,1,2)
            plot(sample_idx+ (c1-1)*(n_samples+5), opt_trial_2(sample_idx), 'color', d1(c1,:), 'LineWidth', 3)
            
            subplot(3,1,3)
            plot(sample_idx+ (c1-1)*(n_samples+5), gt_trial(sample_idx), 'color', d1(c1,:), 'LineWidth', 3)
        else
            subplot(3,1,1)
            plot(sample_idx + (c1-1)*(n_samples+5), opt_trial_1(sample_idx), 'color', .7*ones(1,3), 'LineWidth', 1)
            
            subplot(3,1,2)
            plot(sample_idx+ (c1-1)*(n_samples+5), opt_trial_2(sample_idx), 'color', .7*ones(1,3), 'LineWidth', 1)
            
            subplot(3,1,3)
            plot(sample_idx+ (c1-1)*(n_samples+5), gt_trial(sample_idx), 'color', .7*ones(1,3), 'LineWidth', 1)
        end
    end
end

subplot(3,1,1)
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
ylim([10 50])

subplot(3,1,2)
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
ylim([5 42])


subplot(3,1,3)
patch([1 10 10 1], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([46 55 55 46], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
patch([91 100 100 91], [0 0 500 500], .5*ones(1,3), 'FaceAlpha', .25, 'edgecolor',  'none');
ylim([0 1.2])
%%
subplot(3,1,1)
hold on
plot(y_gt_all)
ylabel('Gamma (norm)')
set(gca,'FontSize', 16)

subplot(3,1,2)
hold on
plot(X_optimal_all(:,1))
% plot(X_samples_all(:,1), '--')
ylabel('Amplitude')
set(gca,'FontSize', 16)

subplot(3,1,3)
hold on
plot(X_optimal_all(:,2))
% plot(X_samples_all(:,2), '--')
xlabel('Samples')
ylabel('Frequency')
set(gca,'FontSize', 16)
drawnow


%%
subplot(3,1,1)
plot(y_gt_all)
ylabel('Gamma (norm)')
set(gca,'FontSize', 16)

subplot(3,1,2)
hold on
plot(X_optimal_all(:,1))
plot(X_samples_all(:,1), '--')
ylabel('Amplitude')
set(gca,'FontSize', 16)

subplot(3,1,3)
hold on
plot(X_optimal_all(:,2))
plot(X_samples_all(:,2), '--')
xlabel('Samples')
ylabel('Frequency')
set(gca,'FontSize', 16)


