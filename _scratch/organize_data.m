clear; 

data_dir        = 'data/';
opto_dir        = [data_dir 'Opto_gamma_optimization/'];

results_dir     = {};
% results_dir     = [results_dir 'EPI032-GP/2018_11_12/'];
% results_dir     = [results_dir 'EPI032-GP/2018_11_15/'];
% results_dir     = [results_dir 'EPI032-GP/2018_11_20/'];
% results_dir     = [results_dir 'EPI032-GP/2018_11_20_2/'];

results_dir     = [results_dir 'EPI033-GP/2018_11_14/'];
% results_dir     = [results_dir 'EPI033-GP/2018_11_16/'];
results_dir     = [results_dir 'EPI033-GP/2018_11_19/'];
results_dir     = [results_dir 'EPI033-GP/2018_11_20/'];

for c1 = 1:size(results_dir,2)
    data_path = [opto_dir results_dir{c1} 'gp_model.mat'];
    load(data_path);
    gp_models{c1} = gp_temp;
end
