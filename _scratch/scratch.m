clear; close all;
figure('Position', [0 0 700 900])
p = [166,206,227;
    178,223,138]/255;

subject = 2;

switch subject
    case 1
        load('data/Opto_gamma_optimization/EPI032-GP/EPI032_all.mat')
    case 2
        load('data/Opto_gamma_optimization/EPI033-GP/EPI033_all.mat')

end

for c1 = 1:size(gp_models,2)
    gp_temp     = gp_models{c1};
    lower_bound = gp_temp.lower_bound;
    upper_bound = gp_temp.upper_bound;

    x_data      = gp_temp.x_data;
    y_data      = gp_temp.y_data;

    n_burn_in   = 5;
    n_samples   = size(x_data,1);
    sample_idx  = n_burn_in+1:n_samples;
    
    for c2 = n_burn_in+1:n_samples
        x_update        = x_data(1:c2,:); 
        y_update        = y_data(1:c2);

        gp_update       = opto_gp_object;
        gp_update.initialize_data(x_update, y_update, lower_bound, upper_bound);

        [x_max, y_max]  = gp_update.discrete_extrema(1.5);

        x_max_est(c2,:) = x_max;
        y_max_est(c2,:) = y_max;

    end

    for c2 = 1:size(x_max_est,2)
        subplot(4,1,c2); hold on
        plot(x_max_est(:,c2), 'color', p(subject,:), 'LineStyle', '-', 'LineWidth', 2)
        plot(sample_idx, x_data(sample_idx,c2), 'color', p(subject,:), 'LineStyle', '--', 'LineWidth', 2)
        set(gca, 'FontSize', 14);
    end
    
    subplot(4,1,4); hold on
    plot(sample_idx, y_max_est(sample_idx), 'color', p(subject,:), 'LineWidth', 2)
    set(gca, 'FontSize', 14);

end

subplot(4,1,1)
ylabel('Amplitude (V)')

subplot(4,1,2)
ylabel('Frequency (Hz)')

subplot(4,1,3)
ylabel('Pulse Width (ms)')

subplot(4,1,4)
ylabel('Gamma (33-50Hz) Power (V^2/Hz)')

