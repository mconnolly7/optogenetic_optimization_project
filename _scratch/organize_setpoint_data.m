%% function organize_setpoint_data
clear

D               = [1.5 3 20];
data_table      = [];

paper_dir       = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/';
experiment_dir  = [paper_dir 'Gamma target (unorganized)/'];
save_dir        = [paper_dir 'data/in_vivo_data/ms_opto_gamma_setpoint/'];

fs              = 2000;
params.tapers   = [3 5];
params.Fs       = fs;
params.fpass    = [33 50];


%% EPI033 EXP 1
baseline        = 2.71*10^-11;
save_name       = 'EPI033_2019_09_30.mat';
date_dir        = [experiment_dir '093019/'];
gp_model_path   = [date_dir 'Gamma150-EPI033_20190930T152243/gp_model.mat'];
intensity_map   = [48.4936, -184.5139];

%% EPI033 EXP 2
baseline        = 2.11*10^-11;
save_name       = 'EPI033_2019_10_01.mat';
date_dir        = [experiment_dir '100119/'];
gp_model_path   = [date_dir 'Gamma150-EPI033_20191001T164708/gp_model.mat'];
intensity_map   = [48.4936, -184.5139];

%% EPI047 EXP 1
baseline        = 6.0648e-11;
save_name       = 'EPI047_2019_11_11.mat';
date_dir        = [experiment_dir '111119/'];
gp_model_path   = [date_dir 'Gamma150-EPI047_20191111T172957/gp_model.mat'];
intensity_map   = [49.6474, -189.0011];

%% EPI047 EXP 2
baseline        = 1.6807e-10;
save_name       = 'EPI047_2019_11_12.mat';
date_dir        = [experiment_dir '111219/'];
gp_model_path   = [date_dir 'Gamma150-EPI047_20191112T162333/gp_model.mat'];
intensity_map   = [49.6474, -189.0011];


%%
optim_path      = [save_dir  'optimization_phase/' save_name];
test_path       = [save_dir  'test_phase/' save_name];
  

%%
for c2 = 1:size(D,2)
    setpoint            = D(c2) * baseline;
    D_string            = num2str(D(c2)*100);

    setpoint_dir        = [date_dir 'Gamma' D_string filesep];
    table_path          = [setpoint_dir 'experiment_table.mat'];
    
    load(table_path)
    load(gp_model_path);
    
    amplitude_sample    = experiment_table.stimulation_amplitude_a*2;
    frequency_sample    = experiment_table.pulse_frequency_pulse;
    pulse_width_sample  = experiment_table.stimulation_pulse_width_pulse_a*2000;
    
    baseline_vec        = baseline * ones(size(amplitude_sample));
    setpoint            = D(c2) * baseline_vec;
    
    
    d                   = dir([setpoint_dir 'Resample*.mat']);
    
    for c1 = 1:size(d,1)
        
        file_name           = d(c1).name;
        file_path           = [setpoint_dir file_name];
        load(file_path)
        
        t_start             = 5 * fs + 1;
        t_end               = t_start + 5*fs;       
        
        stim_data           = data([1:12 14:16],t_start:t_end);
        stim_data           = double(stim_data);
        
        [S F]               = mtspectrumc(stim_data', params);
        
        gamma_sample(c1,1) 	= mean(nansum(S,2),1);
        error_sample(c1,1) 	= -1*abs(gamma_sample(c1,1) - setpoint(c1));
    end
    
    gamma_mult      = D(c2) * ones(size(amplitude_sample,1),1);
    sample_table    = table(gamma_mult, baseline_vec, amplitude_sample, frequency_sample, pulse_width_sample, gamma_sample, error_sample);
      
    lower           = gp_temp.lower_bound;
    upper           = gp_temp.upper_bound;
    
    lower(3)        = lower(3) * 1000;
    upper(3)        = upper(3) * 1000;
    
    X_est           = nan(size(amplitude_sample,1),3);
    Y_est           = nan(size(amplitude_sample,1),1);
    
    for c1 = 10:size(sample_table,1)
        X           = [sample_table.amplitude_sample(1:c1) sample_table.frequency_sample(1:c1) sample_table.pulse_width_sample(1:c1)];
        Y           = error_sample(1:c1);
        
        gp_temp     = opto_gp_object;
        gp_temp.initialize_data(X,Y, lower, upper)
%         gp_temp.minimize();
        
        y                       = gp_temp.predict(gp_temp.t);
        [Y_est(c1,:), Index]    = max(y);
        param                   = gp_temp.t(Index,:);
        X_est(c1,:)             = param;
        
    end
    
    amplitude_est       = X_est(:,1);
    frequency_est       = X_est(:,2);
    pulse_width_est     = X_est(:,3);
    error_est           = Y_est(:,1);
    gamma_est           = Y_est(:,1) + setpoint;
    
    estimate_table      = table(amplitude_est, frequency_est, pulse_width_est, gamma_est, error_est);
    data_table          = [data_table; sample_table, estimate_table];
    
end
save(optim_path, 'data_table');

%% %%%%%%
%
%%%%%%%%%
clear gamma_mult
test_dir            = [date_dir 'Compet/'];
table_path          = [test_dir 'experiment_table.mat'];
    
load(table_path)

amplitude_sample    = experiment_table.stimulation_amplitude_a*2;
frequency_sample    = experiment_table.pulse_frequency_pulse;
pulse_width_sample  = experiment_table.stimulation_pulse_width_pulse_a*2000;
    
d                   = dir([test_dir 'Resampled*.mat']);

amplitudes          = unique(amplitude_sample);


clear psd F gamma_sample
for c1 = 1:size(d,1)
        
    file_name           = d(c1).name;
    file_path           = [test_dir file_name];
    load(file_path)

    t_start             = 5 * fs + 1;
    t_end               = t_start + 5*fs;

    stim_data           = data([1:12 14:16],t_start:t_end);
    stim_data           = double(stim_data);

    params.fpass        = [33 50];
    S                   = mtspectrumc(stim_data', params);
    gamma_sample(c1,1)  = mean(nansum(S,2),1) ;
        
    params.fpass        = [3 100];
    [S, F(c1,:)]        = mtspectrumc(stim_data', params);
    psd(c1,:)           = mean(S,2);
end

gamma_mult(amplitude_sample == amplitudes(1),1) = 150;
gamma_mult(amplitude_sample == amplitudes(2),1) = 300;
gamma_mult(amplitude_sample == amplitudes(3),1) = 2000;

% gamma_mult(amplitude_sample == amplitudes(1),1) = 150 ;
% gamma_mult(amplitude_sample == amplitudes(2),1) = 2000;
% gamma_mult(amplitude_sample == amplitudes(3),1) = 300;

baseline        = baseline * ones(30,1);

test_table      = table(gamma_mult, baseline(1:30), amplitude_sample, ...
    frequency_sample, pulse_width_sample, gamma_sample, psd, F);
save(test_path, 'test_table');


%%
load(optim_path)
intensity_est       = data_table.amplitude_est * intensity_map(1) + intensity_map(2);
intensity_sample    = data_table.amplitude_sample * intensity_map(1) + intensity_map(2);
intensity_table     = table(intensity_sample, intensity_est);
data_table          = [data_table intensity_table];
save(optim_path, 'data_table')

load(test_path)
intensity_sample    = test_table.amplitude_sample * intensity_map(1) + intensity_map(2);
intensity_table     = table(intensity_sample);
test_table          = [test_table intensity_table];
save(test_path, 'test_table')