

param_final = [];
file_name = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/Gamma target (unorganized)/100119/Gamma150-EPI033_20191001T164708/gp_model.mat';
% temp = load('Z:\extracted_data\EPI033\100119\Gamma150-EPI033_20191001T164708\gp_model.mat');
temp                    = load(file_name);
gp_original             = temp.gp_temp;
gp_model                = temp.gp_temp;
xdata                   = gp_original.x_data;
ydata                   = gp_original.y_data;

% gp_temp.initialize();
for i=10:1:length(gp_original.y_data)
    gp_model.x_data     = xdata(1:i,:);
    gp_model.y_data     = ydata(1:i,:);
    y                   = gp_model.predict(gp_model.t);
    [dum Index]         = max(y);
    [dum Index_min]     = min(y);
    [dum Index_zero]    = min(abs(y));
    
    if gp_model.n_vars == 2
        [a b]           = ind2sub([100 100], Index);
        param           = gp_model.t(Index,:)
        param_min       = gp_model.t(Index_min,:)
        param_zero      = gp_model.t(Index_zero,:)
        
    elseif gp_model.n_vars == 3
        [a b c] = ind2sub([100 100 100], Index);
        %     param = gp_model.discrete_aquisition_function(2,0.01)
        param = gp_model.t(Index,:)
        
    end
    param_final(i,:) = param;
end