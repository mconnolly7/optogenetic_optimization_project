%pulse_train_benchtest.m
%
%Shows test stimulation patterns for 4 edge cases fixed in
%opto_generate_train_updated.m

addpath(genpath('D:\Research\Code\optogenetic_optimization_project\code'));

f_sample = 2000;
f_pulse = [35 35 35 105];
f_train = 5;
duration = 8;
duration2 = 20;
amp = 10;
w_train = [1 1 .1 .1];
w_pulse = [.2 .005 .2 .0078333];
n_channels = 1;

figure
suptitle('Less than 10 sec. duration')
for k = 1:4
    subplot(2,2,k)
    stim_plot = opto_generate_train_updated(f_sample, f_train, f_pulse(k), duration, amp, w_train(k), w_pulse(k), n_channels);
    plot(linspace(1,duration,length(stim_plot)),stim_plot);
    title(sprintf('Train f = %.2f, Pulse f = %.2f, Train dur = %.4f, Pulse dir = %.4f',f_train, f_pulse(k), w_train(k), w_pulse(k)));
    xlabel('Time (s)')
end

figure
suptitle('Greater than 10 sec. duration')
for k = 1:4
    subplot(2,2,k)
    stim_plot = opto_generate_train_updated(f_sample, f_train, f_pulse(k), duration2, amp, w_train(k), w_pulse(k), n_channels);
    plot(linspace(1,duration2,length(stim_plot)),stim_plot);
    title(sprintf('Train f = %.2f, Pulse f = %.2f, Train dur = %.4f, Pulse dir = %.4f',f_train, f_pulse(k), w_train(k), w_pulse(k)));
    xlabel('Time (s)')
end