pulse_frequency = linspace(35, 100, 50);
train_frequency = linspace(5,11, 30);

stimulation_params = combvec(pulse_frequency, train_frequency)';

for c1 = 1:size(stimulation_params,1)
    c1
    train_freq = stimulation_params(c1,1);
    pulse_freq = stimulation_params(c1, 2);
    
    params.tapers   = [3 5];
    params.Fs       = 2000;
    a = opto_generate_train(2000, train_freq, pulse_freq , 10, 5, .075, .004, 1  );
    
    [S, F] = mtspectrumc(a, params);
    
    [~, band_start_idx] = min(abs(F - 4));
    [~, band_end_idx]   = min(abs(F - 10));
    
    theta(c1) = sum(S(band_start_idx:band_end_idx));
end

%%
theta_model = gp_object();
theta_model.initialize_data(stimulation_params, theta')
theta_model.plot_mean

%%
data_dir        = '/Users/mconn24/Box Sync/papers/2019_05_01_paper_optogenetic_optimization/data/in_vivo_data/';
experiment_dir  = [data_dir 'ms_opto_theta_maximization_nested_pulse/optimization_data/raw_data/EPI033_040719/Opt_LFP/'];

params.tapers   = [3 5];
params.Fs       = 2000;
params.fpass    = [1 120];
    
d               = dir([experiment_dir 'R*']);
S_all           = [];

for c1 = 1:size(d,1)
    c1
    file_name           = d(c1).name;
    file_path           = [experiment_dir file_name];
    stim_struct         = load(file_path);
    
    data                = stim_struct.data;
    
    [S, T, F]           = mtspecgramc(data(4,:), [10 1], params);
    
    S_all               = [S_all; S];
end


subplot(2,1,1)
plot_matrix(S_all, 1:size(S_all,1), F)
colormap('jet')

[~, band_start_idx] = min(abs(F - 4));
[~, band_end_idx]   = min(abs(F - 10));
theta               = sum(S_all(:,band_start_idx:band_end_idx),2);
subplot(2,1,2)
plot(theta)

